#!/bin/bash

for component in components/*
do
	(
		cd $component
		npm i
		grunt build
	)
done