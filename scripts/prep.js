#! /usr/bin/env node

/* Warning! you are now entering CALLBACK HELL. This script contains a bunch of assumptions. If
   you're not sure why you'd run it, don't. */


var child_process = require( 'child_process' ),
	fs = require( 'fs' ),
	path = require( 'path' ),

	pkg,
	repo_name;


// read package.json
fs.readFile( path.resolve( __dirname, '..', 'package.json' ), function ( err, result ) {
	if ( err ) {
		throw err;
	}

	pkg = JSON.parse( result.toString() );

	install_dependencies( function () {
		create_remote_repo( function ( result ) {
			create_local_repo( function () {
				console.log( 'Preparation complete. Run `grunt` to start grunting your project.' );
				process.exit();
			});
		});
	});
});



function install_dependencies ( callback ) {
	var child;

	console.log( '\n\nInstalling grunt and related dependencies. This may take 30 seconds or so\n=========================================================================\n' );

	child = child_process.exec( 'npm install', function ( error, stdout, stderr ) {
		if (error) {
			console.log(error.stack);
			console.log('Error code: '+error.code);
			console.log('Signal received: '+error.signal);
		}

		else {
			process.stdout.write( 'done\n' );
			callback();
		}
	});

	child.stdout.on('data', function (data) {
		process.stdout.write( '.' );
	});

	child.stderr.on('data', function (data) {
		process.stdout.write( '.' );
	});
}



function create_remote_repo ( callback ) {

	console.log( '\n\nCreating remote BitBucket repository\n====================================\n' );

	repo_name = resolve_name( pkg.name );

	prompt( 'Enter your BitBucket username (or hit enter if you don\'t want to create a remote repo at this time)', function ( bbUser ) {
		if ( !bbUser ) {
			repo_name = null;
			callback( false );
			return;
		}

		prompt( 'Enter your BitBucket password', function ( bbPass ) {

			prompt( 'Enter repository name, or hit Enter for ' + repo_name, function ( name ) {
				var cmd, result;

				repo_name = name || repo_name;

				cmd = 'curl --user ' + bbUser + ':' + bbPass + ' ' +
				      'https://api.bitbucket.org/1.0/repositories/ --data name="' + repo_name +'" ' +
				      '--data is_private="true" --data owner="gia_src"';

				child = child_process.exec( cmd, function ( error, stdout, stderr ) {
					if ( error ) {
						console.log( error.stack );
						console.log( 'Error code: ' + error.code );
						console.log( 'Signal received: ' + error.signal );
					}

					else {
						if ( result ) {
							console.log( 'Successfully created remote repo' );
							callback();
						} else {
							prompt( '\nFailed to create remote repo. Retry? [y/n]', function ( answer ) {
								if ( !( /^y/i.test( answer ) ) ) {
									console.log( 'Quitting' );
									process.exit();
								} else {
									create_remote_repo( callback );
								}
							});
						}
					}
				});

				child.stdout.on( 'data', function (data) {
					try {
						result = JSON.parse( data );
					} catch ( err ) {
						console.error( data );
					}
				});
			});

		}, { hidden: true });
	});

}

function create_local_repo ( callback ) {

	if ( !repo_name ) {
		callback();
		return;
	}

	console.log( '\n\nCreating local repository\n=========================\n' );

	var cmds, exec_next_command;

	cmds = [
		'git init',
		'git add -A',
		'git commit -m "initial commit"',
		'git remote add origin git@bitbucket.org:gia_src/' + repo_name +'.git',
		'git push -u origin --all'
	];

	exec_next_command = function () {
		var cmd;

		cmd = cmds.shift();

		if ( !cmd ) {
			callback();
			return;
		}

		child = child_process.exec( cmd, function ( error, stdout, stderr ) {
			if ( error ) {
				console.log( error.stack );
				console.log( 'Error code: ' + error.code );
				console.log( 'Signal received: ' + error.signal );

				process.exit();
			}

			else {
				exec_next_command();
			}
		});

		// child.stdout.on( 'data', function (data) {
		// 	console.error( data );
		// });
	};

	exec_next_command();

}


function prompt ( message, callback, options ) {
	var answer = '', data_handler;

	options = options || {};

	process.stdin.resume();
	process.stdin.setEncoding( 'utf8' );

	process.stdout.write( message + '\n' );
	process.stdout.write( '> ' );

	if ( !!options.hidden ) {
		process.stdin.setRawMode( true );

		data_handler = function ( char ) {
			switch (char) {
				case "\n": case "\r": case "\u0004":
				// They've finished typing their password
				process.stdin.setRawMode( false )
				process.stdin.pause();

				process.stdin.removeListener( 'data', data_handler );
				process.stdout.write( '\n' );
				callback( answer );
				break

				case "\u0003":
				// Ctrl C
				console.log( 'Cancelled' );
				process.exit();
				break

				default:
				// More passsword characters
				process.stdout.write( '*' );
				answer += char;
			}
		};
	}

	else {
		data_handler = function ( line ) {
			process.stdin.removeListener( 'data', data_handler );
			callback( line.replace( /\r?\n$/, '' ) );
		};
	}

	process.stdin.on('data', data_handler );
}



function resolve_name ( name ) {
	var date;

	if ( /^20/.test( name ) ) {
		return name;
	}

	date = new Date( pkg.created );

	return date.getFullYear() + '-' + pad( date.getMonth() + 1, 2 ) + '-' + name;
}

function pad ( num, digits ) {
	var str = '' + num;

	while ( str.length < digits ) {
		str = '0' + str;
	}

	return str;
}
