/* The one-size-fits-all key to Grunt.js happiness - http://bit.ly/grunt-happy */

/*global module:false*/
module.exports = function ( grunt ) {

	'use strict';

	var config, dependency;

	config = {
		pkg: grunt.file.readJSON( 'package.json' ),

		// Make sure this is what you expect it to be!
		projectPath:  'next-gen/environment/ng-interactive/2014/mar/-sp-toxic-waste-silicon-valley-trail',

		// Low-level config. It's very unlikely that you need to touch this
		stage:	 grunt.option( 'stage' ),
		prod:	  grunt.option( 'prod' ),
		min:	   grunt.option( 'min' ) || grunt.option( 'prod' ),
		dryrun:	grunt.option( 'dryrun' ),
		target:	'<%= min ? "min" : "dev" %>',
		tmpTarget: '<%= min ? "tmp" : "dev" %>',

		// Template tags. Any time you use these tags in your code, they will
		// be evaluated by grunt - e.g. you can do things like
		//
		//	 .widget {
		//	   background-image: url(<%= projectUrl %>/<%= versionDir %>/icons/widget.png)
		//	 }
		//
		// This allows you to use URLs that are correct when the interactive
		// is inserted into an arbitrary page. It also allows you to use offline
		// resources during development, but CDN resources in production
		codeobject:  grunt.file.read( 'preview/codeobject.html' ),
		require:	 '<%= prod ? "http://pasteup.guim.co.uk/js/lib/requirejs/2.1.5/require.min.js" : "../../offline/require.js" %>',
		curljs:	 '<%= prod ? "http://pasteup.guim.co.uk/js/lib/requirejs/2.1.5/require.min.js" : "../../offline/curl.js" %>',
		projectUrl:  '<%= prod ? baseUrl + "/" + projectPath : "." %>',
		versionDir:  'v/<%= version %>',
		fonts: '<%= stage ? "http://gia-stage.s3-website-us-east-1.amazonaws.com/fonts/fonts.css" : ( prod ? "http://pasteup.guim.co.uk/0.0.5/css/fonts.pasteup.min.css" : "../../offline/fonts.css" ) %>',
		assets: '<%= baseUrl %>/<%= projectPath %>/assets',

		components: '<%= projectUrl %>/<%= versionDir %>/components',
		componentPrefix: '<%= prod ? (projectUrl + "/components") : "../../../../components" %>',
        componentPostfix: '<%= prod ? "" : "/build/dev" %>',

        article: (function () {
		    var marked, markdown, html;

		    marked = require( 'marked' );

		    markdown = grunt.file.read( 'src/versioned/article/article.md' ).replace( /\$\{\s*([^\}]+)\s*\}/g, function ( match, include ) {
		        return grunt.file.read( 'src/versioned/article/includes/' + include + '.html' );
		    });

		    html = marked( markdown );

		    return html;
		}()),

		// Deployment-related stuff
		guid: 'f3da7cea-ff2b-4c69-bf9c-ba2763c83433',
		baseUrl: '<%= stage ? "http://gia-stage.s3-website-us-east-1.amazonaws.com" : "http://interactive.guim.co.uk" %>',
		version: 'x', // will be overridden by deploy task

		// Deployment credentials
		aws_uk_credentials: tryToLoadCredentials( '/Users/' + process.env.USER + '/.gia/aws-uk.json' ),
		aws_us_credentials: tryToLoadCredentials( '/Users/' + process.env.USER + '/.gia/aws-us.json' ),

        // inline_data should be true if you want the contents of the `data` folder to be accessible as
        // an AMD module, i.e. `data = require('data')`. Otherwise a `data.json` file will be created,
        // which you can load via AJAX
        inline_data: true,

		// Fetch external resources, e.g. csv from a Google Spreadsheet
		curl: {
			// 'src/versioned/data/table.csv': 'https://docs.google.com/spreadsheet/pub?key=0AgS_UxBggDCMdxxxxxxxxFM1ME5aMjFweWRpQnc&single=true&gid=1&output=csv'
		}
	};




	// Read config files from the `grunt/config/` folder
	grunt.file.expand( 'grunt/config/*.js' ).forEach( function ( path ) {
		var property = /grunt\/config\/(.+)\.js/.exec( path )[1],
			module = require( './' + path );
		config[ property ] = typeof module === 'function' ? module( grunt ) : module;
	});

	// Initialise grunt
	grunt.initConfig( config );

	// Load development dependencies specified in package.json
	for ( dependency in config.pkg.devDependencies ) {
		if ( /^grunt-/.test( dependency) ) {
			grunt.loadNpmTasks( dependency );
		}
	}

	// Load tasks from the `grunt-tasks/` folder
	grunt.loadTasks( 'grunt/tasks' );

};



// This prevents missing credentials files from borking things up
function tryToLoadCredentials ( filepath ) {
	var credentials;

	try {
		credentials = JSON.parse( require( 'fs' ).readFileSync( filepath ).toString() );
	} catch ( err ) {
		console.log( 'Missing credentials file ' + filepath + '. You will not be able to deploy the project using Grunt until this file is created' );
		credentials = {};
	}

	return credentials;
}
