offline
=======

You can pretty much ignore this folder.

It contains a few things that are useful for previewing the project locally - e.g. we need require.js, but a) we don't want to include it in our project since the website already has a module loader, and b) we don't want to load it from a CDN with every page reload during development if the cache is disabled.
