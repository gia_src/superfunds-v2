Grunt task config
=================

This folder contains configuration for the various grunt tasks that this project uses. In most cases, you won't need to edit them - but you are strongly encouraged to do so if necessary.
