module.exports = {
	files: [
		'src/versioned/js/**/*.js',

		//exclude these files:
		'!src/versioned/js/lib/**/*.js',
		'!src/versioned/js/utils/**/*.js',
		'!src/versioned/js/loaders/**'
	],
	options: {
		undef: true,
		unused: true,
		boss: true,
		smarttabs: true,
		globals: {
			define: true,
			window: true,
			document: true,
			navigator: true,
			XMLHttpRequest: true,
			setTimeout: true,
			clearTimeout: true,
			Image: true
		},

		// don't actually fail the build
		force: true
	}
};
