module.exports = {
	options: {
		compress: {
			dead_code: true,
			conditionals: true // e.g. rewrite `if ( <%= production %> ) { doX(); } else { doY() }` as `doX()`
		}
	},
	build: {
		files: [{
			expand: true,
			cwd: 'build/min/',
			src: '**/*.js',
			dest: 'build/min/'
		}]
	}
};
