module.exports = {
	build: {
		files: [{
			expand: true,
			cwd: 'build/min/',
			src: '**/*.css',
			dest: 'build/min/'
		}]
	}
};
