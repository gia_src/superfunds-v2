module.exports = {
	sass: {
		files: 'src/**/*.scss',
		tasks: 'sass'
	},
	data: {
		files: 'src/versioned/data/**/*',
		tasks: 'spelunk'
	},
	versioned: {
		files: [ 'src/versioned/**/*', '!src/versioned/js/**/*' ],
		tasks: 'copy:versioned'
	},
	nonVersioned: {
		files: [ 'src/**/*', '!src/versioned/**/*' ],
		tasks: 'copy:nonVersioned'
	},
	js: {
		files: 'src/versioned/js/**',
		tasks: 'copy:js'
	},

	components: {
		files: 'components-src/**/*.js',
		tasks: 'buildComponents'
	}
};
