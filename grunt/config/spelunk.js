// Config for grunt-spelunk. By default (i.e. `useAmd === true`) it will
// create a `data.js` module that you can use in your project with
//
//     data = require( 'data' );
//
// If you pass `false` instead, it will create a `data.json` file, which
// you can load with
//
//     $.getJSON( '<%= projectUrl %>/<%= versionDir %>/data.json' ).then( function ( result ) {
//       data = result;
//     });
module.exports = {
	toxictrail: {
		root: 'components-src/toxictrail/data/',
		dest: 'components-src/toxictrail/js/data.js',
		options: {
			exclude: [ '**/README.md' ],
			space: '<%= min ? "" : "\t" %>',
			amd: true
		}
	},

	oversight: {
		root: 'components-src/oversight/data/',
		dest: 'components-src/oversight/js/oversight_data.js',
		options: {
			exclude: [ '**/README.md' ],
			space: '<%= min ? "" : "\t" %>',
			amd: true
		}
	}
};
