module.exports = {
	options: {
		accessKeyId: '<%= stage ? aws_us_credentials.key : aws_uk_credentials.key %>',
		secretAccessKey: '<%= stage ? aws_us_credentials.secret : aws_uk_credentials.secret %>',
		bucket: '<%= stage ? "gia-stage" : "gdn-cdn" %>',
		access: 'public-read',
		debug: '<%= dryrun %>',
		params: {
			CacheControl: 'max-age=20'
		}
	},

	downloadManifest: {
		action: 'download',
		cwd: 'tmp/old/',
		dest: '<%= projectPath %>/manifest.json',
		options: {
			debug: false // regardless of whether we're in dryrun mode
		}
	},

	/*uploadRoot: {
		files: [{
			expand: true,
			cwd: 'build/<%= target %>/',
			src: '*',
			dest: '<%= projectPath %>'
		}]
	},

	uploadVersion: {
		files: [{
			expand: true,
			cwd: 'build/<%= target %>/v/x/',
			src: '**',
			dest: '<%= projectPath %>/<%= versionDir %>'
		}],
		options: {
			params: {
				CacheControl: 'max-age=31536000'
			}
		}
	},*/

	uploadRootZip: {
		files: [{
			expand: true,
			cwd: 'build/zip/',
			src: [ '*.js', '*.json', '*.csv', '*.css', '!**/manifest.json' ],
			dest: '<%= projectPath %>'
		}],
		options: {
			params: {
				ContentEncoding: 'gzip',
				CacheControl: 'max-age=20'
			}
		}
	},

	uploadVersionZip: {
		files: [{
			expand: true,
			cwd: 'build/zip/v/x/',
			src: [ '**/*.js', '**/*.json', '**/*.csv', '**/*.css' ],
			dest: '<%= projectPath %>/<%= versionDir %>'
		}],
		options: {
			params: {
				CacheControl: 'max-age=31536000',
				ContentEncoding: 'gzip'
			}
		}
	},

	uploadRootNonZip: {
		files: [{
			expand: true,
			cwd: 'build/zip/',
			src: [ '*', '!*.js', '!*.json', '!*.csv', '!*.css', '**/manifest.json' ],
			dest: '<%= projectPath %>'
		}]
	},

	uploadManifest: {
		files: [{
			expand: true,
			cwd: 'build/zip/',
			src: [ 'manifest.json' ],
			dest: '<%= projectPath %>'
		}]
	},

	uploadVersionNonZip: {
		files: [{
			expand: true,
			cwd: 'build/zip/v/x/',
			src: [ '**', '!**/*.js', '!**/*.json', '!**/*.csv', '!**/*.css' ],
			dest: '<%= projectPath %>/<%= versionDir %>'
		}],
		options: {
			params: {
				CacheControl: 'max-age=31536000'
			}
		}
	},

	uploadAssets: {
		files: [{
			expand: true,
			cwd: 'assets/',
			src: '**',
			dest: '<%= projectPath %>/assets/'
		}],
		options: {
			params: {
				CacheControl: 'max-age=31536000'
			}
		}
	}
};
