module.exports = {
	js: {
		options: {
			mode: 'gzip'
		},
		expand: true,
		cwd: 'build/min/',
		src: ['**/*.js'],
		dest: 'build/zip/',
		ext: '.js'
	},

	json: {
		options: {
			mode: 'gzip'
		},
		expand: true,
		cwd: 'build/min/',
		src: ['**/*.json', '!manifest.json'],
		dest: 'build/zip/',
		ext: '.json'
	},

	csv: {
		options: {
			mode: 'gzip'
		},
		expand: true,
		cwd: 'build/min/',
		src: ['**/*.csv'],
		dest: 'build/zip/',
		ext: '.csv'
	},

	css: {
		options: {
			mode: 'gzip'
		},
		expand: true,
		cwd: 'build/min/',
		src: ['**/*.css'],
		dest: 'build/zip/',
		ext: '.css'
	}
};
