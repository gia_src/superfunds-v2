module.exports = function ( grunt ) {

	'use strict';

	grunt.registerTask( 'buildArticle', function () {
		var script, target;

		script = grunt.template.process( 'document.getElementById("gia-superfunds").innerHTML = <%= JSON.stringify(article) %>;' );
		target = grunt.template.process( 'build/<%= target %>/v/x/js/app.js' );

		grunt.file.write( target, script );
	});

};
