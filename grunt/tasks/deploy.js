module.exports = function ( grunt ) {

	'use strict';

	// launch sequence
	grunt.registerTask( 'deploy', [
		'clean:tmp',

		// set production flag
		'setProdFlag',

		// connect to S3, establish version number
		'aws_s3:downloadManifest',
		'incrementVersion',

		// build project
		'min',

		// gzip project
		'copy:min_to_zip',
		'compress',

		// upload files
		'aws_s3:uploadRootZip',
		'aws_s3:uploadVersionZip',
		'aws_s3:uploadRootNonZip',
		'aws_s3:uploadVersionNonZip',
		'aws_s3:uploadManifest',

		// point browser at newly deployed project
		'shell:open'
	]);

	grunt.registerTask( 'stage', [
		'setStageFlag',
		'deploy'
	]);
};
