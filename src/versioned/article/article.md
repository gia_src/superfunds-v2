<div class='gia-header'>
	<img src='<%= assets %>/images/toxictrail-logo.gif'>
</div>

<div class='gia-article-intro'>
	<p class='gia-article-intro-large'>The landmark Superfund program is supposed to clean up the country’s toxic waste. But as one site in Silicon Valley shows, it’s leaving behind its own legacy of environmental problems.</p>
	<p class='gia-byline'>By Susanne Rust and Matt Drange, [The Center for Investigative Reporting,](http://cironline.org/toxictrail/partner?partnername=Guardian) and the Guardian US Interactive Team</p>
</div>


${chapters}


Below some of the world’s most expensive real estate, in the heart of Silicon Valley, pipes and pumps suck thousands of gallons of contaminated water every hour from vast underground toxic pools.

Giant industrial filters trap droplets of dangerous chemicals at the surface, all in the hope of making the water drinkable again and protecting the workers of tech giants such as Google and Symantec from toxic vapors.


${cir}


But that costly journey to the surface is only the start of a toxic trail with no clear end.

Once it leaves Mountain View, California, the toxic waste gets shipped, treated and burned in places like Oklahoma and Arizona, discharging waste in small towns and on a Native American reservation, and in some cases creating even more harmful chemicals, [The Center for Investigative Reporting](http://cironline.org/toxictrail/partner?partnername=Guardian) has found.

Along the way, waste treatment plants rack up environmental violations, records show. Byproducts created during treatment are shuttled from one plant to another. And then another. After crisscrossing the country, the waste even can end up right back where it started – at a treatment plant just a few miles away in Silicon Valley.

It’s a shell game in which one environmental danger appears to be addressed, yet is moved somewhere else in the form of a new problem.

“There’s really no such thing as throwing something away,” said Environmental Protection Agency spokesman Rusty Harris-Bishop. “You’re always throwing it somewhere.”


${toxictrail}


The EPA pays close attention to the more than 1,300 toxic sites that constitute its landmark Superfund program. But the toxic trail highlights a key gap: After the waste-hauling trucks rumble out of town, the EPA considers the job to be finished.

As a result, the country’s environmental regulators are creating their own legacy of unintended consequences as they grapple with the mess left behind by a previous generation. Along the trail, contained toxic waste is turned into an array of uncontrolled and potentially worse problems that fan out across the U.S.

Often the original mess is almost untreatable. In Silicon Valley’s case, it would take [700 years of continuous treatment](https://www.documentcloud.org/documents/1029451-mew-700years-report.html#document/p27/a146244) to make the groundwater drinkable.

The EPA [recognizes that the trail exists](https://www.documentcloud.org/documents/1026251-epa-green-remediation.html#document/p15/a146207) but says it’s too difficult to follow.

“It’s not that we don’t care about the material,” said Carlos Pachon, who leads the EPA’s green cleanup efforts. “We just don’t have control over it.”

[CIR](http://cironline.org/toxictrail/partner?partnername=Guardian) constructed a composite picture of the trail’s environmental toll by piecing together hazardous waste shipping documents, company records, environmental violations and scientific studies. It’s a first-of-its kind accounting of the hidden impacts of a widely lauded cleanup effort, and it highlights the challenges facing the nation’s Superfund program.


<figure class='gia-photo gia-photo-fullwidth'>
	<img src='<%= assets %>/images/photos/photo_full02.jpg'>
	<figcaption>Since the 1980s, a technology called “pump and treat” has been used to clean contaminated groundwater at the site in Silicon Valley. Photo: Noah Berger</figcaption>
</figure>


Among the findings:

* **Waste begets waste.** At every step along the trail, treatment leaves behind a new batch of waste that needs to be shipped somewhere else. At one stop, a plant in Wisconsin creates more waste than it takes in.

* **Treatment creates new hazards.** The superheating used to release toxic chemicals gives way to an equally alarming danger that isn’t monitored: dioxins. After they escape the plants, dioxins can build up in the food supply and have been linked to cancer and birth defects.

* **The system is highly inefficient.** For every 5 pounds of contaminants pulled from the ground, roughly 20,000 pounds of carbon dioxide are produced from continually running pumps, cross-country treks and treatment plants that produce as much greenhouse gas as municipal power plants.

* **Cleanup at the Silicon Valley site, and others like it, isn’t working.** Over the past decade, pollution levels there have remained stagnant despite constant pumping. In some cases, the treatment is actually increasing the pollution in the water.

* **The costs of treating the waste are enormous.** To continue cleanup at sites like this, the [EPA estimates](https://www.documentcloud.org/documents/1026474-epa-p-amp-t-cost.html#document/p4/a146212) taxpayers will spend up to $1.2 billion for every 10 years of ongoing treatment. That doesn’t include the untallied billions more spent by private companies tasked with cleaning up their past messes.


The Silicon Valley site is not an anomaly. There are more than 450 other Superfund sites like it. They have contaminated groundwater, and the cleanup is complicated by different types of soils that contain hard-to-clean chemicals. Experts have lost faith in the technology used to clean them up.

That means that as many as one-third of all Superfund sites could be causing more environmental harm than good.

“Sometimes you say, ‘We don’t have a good solution for this,’ ” said Sarah Stafford, a professor at The College of William and Mary who has researched the effects of environmental regulations on hazardous waste management. “So, let’s minimize the problem, monitor it and wait to figure out what in the world we’re going to do with this waste.”


${map}


## Genesis of toxic groundwater

The evolution of the tech industry can be traced through the Mountain View site’s inhabitants.

First, Intel and Fairchild Semiconductor made computer chips in the 1960s and ’70s, giving Silicon Valley its name. Then AOL and Netscape moved in and helped shape the Internet during the 1990s and early 2000s. Now, Symantec calls it home. Google has a satellite campus here, just a few miles south of its headquarters down Silicon Valley’s main artery, Highway 101.

Back when Intel and Fairchild were making the first mass-produced computer chips, they used solvents such as trichloroethylene, or TCE, and benzene to degrease the chips. The cancer-causing chemicals leaked into the ground and polluted the soil and water below. The companies now are responsible for cleaning it up.

Today, a swimming pool is being constructed alongside shiny new buildings and massive outdoor sculptures. Tree-canopied walkways lead from one high-tech campus to the next, and the only indication as to what lies below is the steady whir of pumps, strategically placed around the site to suck the polluted groundwater out of the earth. This is known as “pump and treat,” a pillar of Superfund cleanup efforts.


${video}


The Superfund program began in 1980, after a valley of leaking chemicals in the small upstate New York community of Love Canal drew attention to the dangers of living near toxic waste. The program provided funding and the infrastructure to clean up toxic sites across the country.

In most cases, the companies responsible for the pollution pay the tab. Intel, Raytheon and Fairchild are long gone from the Silicon Valley site, for example, but are still footing the bill for cleanup. They don’t have to disclose how much they spend. But one [internal presentation](https://www.documentcloud.org/documents/1034064-mew-site.html#document/p3/a147107) from the companies noted that the costs to clean the site surpassed $100 million from the early 1980s to 2003.

To tell this story, [CIR](http://cironline.org/toxictrail/partner?partnername=Guardian) followed one route taken by one stream of waste after it left Silicon Valley.

The first stop on the trail: the Calgon Carbon Corporation’s Big Sandy plant. It’s a rusty, incongruous complex of smoke stacks, conveyor belts and sheet metal sitting along the western bank of the Big Sandy River in Kentucky, more than 2,500 miles from Mountain View.

Once a shipment of hazardous waste arrives at Calgon, it’s unloaded into a pit. From there it’s fed into a towering furnace that burns day and night, every day of the year. The extreme heat inside approaches 2,000 degrees, separating the toxic chemicals from the filters.

The process leaves behind toxic ash and other newly contaminated filters.

For every 5 pounds of hazardous waste that arrives here, [an additional pound of waste](https://www.documentcloud.org/documents/1030069-2011-biennial-state-rcra-report.html#document/p153/a146314) is created that needs to be shipped to a different plant.

There are also the pollutants that escape into the air and water.

One of those is a class of chemicals called dioxins, which are on the EPA’s list of  [“dirty dozen”](http://www.epa.gov/oia/toxics/pop.html#/h) dangerous chemicals.

Dioxins form during the superheating process and can escape the plant through vents and openings. They eventually trickle down into water, soils and plants. People are exposed as the chemicals build up in the food supply. Animals such as cows, chicken and fish eat contaminated grass or feed, and then people eat those animals.


${dioxins}


Dioxins have been linked to cancer and reproductive harm in humans and animals and are prevalent in the environment and food supply. For instance, [one study showed](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3261987) that women in Seveso, Italy, who were exposed to high levels of dioxins after a chemical explosion were significantly more likely to have cancer. Other studies have shown [higher levels of tumors](https://www.documentcloud.org/documents/1030092-epa-dioxin-report.html#document/p4/a146317) in the liver, lung, tongue and thyroid in animals that ate food contaminated with dioxins.

People don’t need to live near an incinerator to be exposed to dioxins, which can be [carried great distances](https://www.documentcloud.org/documents/1034056-dioxins-and-wind-patterns.html#document/p1/a147096) by the wind. For instance, the Inuit, people native to the Arctic Circle, live thousands of miles from most dioxin sources. But they have some of the world’s highest concentrations of the contaminants in their bodies because their diet is rich in animal fat.

The handful of treatment plants that handle waste like Silicon Valley’s are significant sources of dioxins, the EPA says. But it’s difficult to measure precisely how much dioxin they contribute to the environment. The plants aren’t required to report releases of dioxins and regulators haven’t [researched the issue since 1987.](https://www.documentcloud.org/documents/1034023-2013-dioxin-report.html#document/p198/a147079)

Bart Schaffer, supervisor of the corrective action section of Kentucky’s hazardous waste branch, said the state’s regulators don’t focus on dioxins released by facilities like Calgon, but they could soon. He compared it to the lack of attention once given to the chemicals that are now being removed from Silicon Valley.

“It’s just not something we are familiar with,” Schaffer said. “It’s like TCE in the 1970s.”


<figure class='gia-photo gia-photo-fullwidth'>
	<img src='<%= assets %>/images/photos/photo_full01.jpg'>
	<figcaption>The Calgon Carbon Corp.’s plant in Catlettsburg, Kentucky, is one of a handful of plants that handles waste from Superfund sites that use pump-and-treat technology. Photo: Geoff Bugbee</figcaption>
</figure>


## Environmental violators

To clean up places like Silicon Valley, waste gets sent to plants that have violated the Clean Water Act, the Clean Air Act and other environmental laws.

Last year, [Calgon agreed to pay $1.6 million](https://www.documentcloud.org/documents/1031419-calgon-1-6-million-settlement.html#document/p1/a146662) to settle charges that it sold hazardous waste byproducts instead of disposing of them properly. The company did not admit to wrongdoing.

On top of that, facility operators dumped [540,000 gallons of hazardous waste](https://www.documentcloud.org/documents/1030119-kyepa2012.html#document/p13/a146341) into the Big Sandy River in 2011. Between 2009 and 2011, [Calgon polluted the river](https://www.documentcloud.org/documents/743530-denfnov100511-rtf-12257depc388758.html) with oil, grease and fecal coliform, state records show. On four separate occasions, the company was cited for exceeding its pollution limits. In each case, state regulators found no direct harm to people or the environment.

Marv Church, Calgon’s environmental and safety director, said the company didn’t conduct follow-up testing.

“There was nothing to test. There was a black streak in the river, and once it was gone it was gone,” he said.

The Big Sandy plant is one of four carbon regeneration facilities licensed to treat hazardous waste in the United States. Two other plants are in Pennsylvania – one in Darlington and another on Neville Island outside Pittsburgh. The fourth is on Native American tribal land outside Parker, Arizona. They all accept and treat Superfund waste from across the country.

Back in Mountain View, the companies have their pick of which facility gets their waste. The companies don’t have to disclose where they send it, but company records shed some light on the flow.

Since 2009, Calgon’s Big Sandy plant has accepted 25 tons of hazardous waste from the site. Calgon said it doesn’t know exactly how much of the waste it receives comes from Superfund sites as a whole, but current and former employees estimate it’s about 10 percent of all waste that arrives there annually.

Calgon has accumulated more state and federal environmental violations in the past five years than its competitors. But these plants are just the first stop in this trail of waste.

The Arizona plant accepts the bulk of hazardous waste from the Silicon Valley site. It has only minor violations. But it sends the waste it generates to the Clean Harbors Aragonite Incineration facility in rural Utah, which has a [lengthy track record](https://www.documentcloud.org/documents/745701-aragonite-nov.html) of environmental violations.


${proliferation}


Since 2003, Aragonite has paid the state more than $1 million in fines for routinely losing track of waste shipments, failing to address problems that led to 15 fires in one year and a slew of other violations.

"Yes, from time to time we have violations. We're humans, and humans make mistakes," said Phil Retallick, senior vice president of regulatory affairs for Clean Harbors Environmental Services, which runs Aragonite. "There are some problems, but we try our best to fix them."

Calgon sends most of its waste 300 miles north to the Detroit suburb of Belleville. The Michigan Disposal Waste Treatment Plant was on the [EPA’s Watch List](https://www.documentcloud.org/documents/1030295-june-2013-rcra-watch-list.html#document/p3/a146415) for suspected chronic violators of environmental laws from October 2012 through last June. Then, [owners paid the federal agency nearly $400,000](https://www.documentcloud.org/documents/1031420-michigan-disposals-settlement.html) to settle violations of its hazardous waste permit. Inspectors found the facility disposed of hazardous waste without properly treating it.

Such violations, while not desired, are a natural consequence of the waste treatment industry, a spokesman for Michigan Disposal said.

“We’re trying to do a good thing for the environment,” David Crumrine said. “There’s no way to snap your fingers and make this waste go away.”


## Road to toxic cleanup

Every month, blue tanker trucks with Calgon’s logo emblazoned on the side begin a northward journey, through the main streets of Ashland, Kentucky, past its pawnshops, vacant apartment buildings, dingy negligee shops and fast-food restaurants. Once across the state border, the road winds through central Ohio’s expanse of rest stops, agricultural fields, mega-porn shops and arena-sized churches.

Nestled between Interstate 94 and the Willow Run Airport, the 500-acre Michigan plant and landfill sit just off a rutted asphalt drive warped by the constant stream of 18-wheelers trampling over it. On Monday mornings, trucks idle outside the facility waiting to dump waste.

Here, the ash and dirty filters from Calgon undergo the next round of treatment. In some cases, the waste is infused with chemicals that break it apart. In others, it’s put in a hot furnace and blasted with oxygen to create less harmful chemicals.

The waste is mixed with thousands of other loads of hazardous waste, including some laced with mercury.

These methods, in turn, create their own hazardous byproducts. Just as Calgon’s superheating process can release dioxins, so, too, does the heating used in thermal oxidation.

In addition, for every 5 pounds of waste that arrives at the Michigan plant, [2.75 pounds gets created.](https://www.documentcloud.org/documents/1030069-2011-biennial-state-rcra-report.html#document/p193/a146322) The amount of waste is reduced. But it’s not eliminated.

Some of those leftovers are sent to another plant in Union Grove, Wisconsin, which then sends its own waste somewhere else. For every 5 pounds of waste that arrives in Wisconsin, nearly [6 pounds of new waste leaves,](https://www.documentcloud.org/documents/1030069-2011-biennial-state-rcra-report.html#document/p446/a146327) according to federal documents.

The plant specializes in mercury removal. It’s an energy-intensive process that only removes a small amount of the material. Because mercury is so toxic, anything that comes into contact with it becomes hazardous. Things like workers’ aprons get added to the stream of hazardous waste.

“They are reducing the toxicity of the waste. Even if they end up increasing the volume of waste that came in, as long as they are reducing the toxicity we’re OK with it,” said Mike Ellenbecker of the state’s Department of Natural Resources.

Ultimately, the trail doesn’t have a single end point. Plants continually accept, treat, generate and then ship waste elsewhere to any number of different destinations. The result is a weblike network that spans the United States. On a map, it looks like fireworks bursting from treatment plants, igniting new sparks as it moves along the trail.


<figure class='gia-photo gia-photo-fullwidth'>
	<img src='<%= assets %>/images/photos/new_photo_full03.jpg'>
	<figcaption>The superheating process used at Calgon Carbon Corp.’s plant can release dioxins, which have been linked to cancer and birth defects, into the air. Photo: Geoff Bugbee</figcaption>
</figure>


Some of the waste takes a meandering path right back to Silicon Valley. The Aragonite plant in Utah sends some of its waste to a treatment plant in San Jose just down the highway from the Silicon Valley site.

The waste often ends up being buried back in the ground at a landfill. Even then, landfills give off their own byproducts, such as toxic runoff, that must be treated and shipped to plants similar to Calgon’s in Kentucky.

That starts the whole process over again.

“We’re talking about a toxic merry-go-round that spins around and around,” said Stephen Lester, a science director for the Center for Health, Environment and Justice. The group was created by a resident of the Love Canal neighborhood in New York whose advocacy helped spur the Superfund law.


## Winding trail of carbon footprints

Each step along the trail comes with its own carbon footprint.

First there is the energy required to pull the waste out of the ground. For all the pump-and-treat cleanup systems nationally, the EPA estimates that [more than 356,000 tons of carbon dioxide](https://www.documentcloud.org/documents/1026901-epa-energy-carbon-footprint.html#document/p10/a146213) are produced each year. That’s about the same amount emitted by 16,000 U.S. homes annually.

Then there’s the shipping footprint of all that waste once it’s removed.

For every shipment of waste sent from Mountain View to Kentucky, for example, more than 3 tons of carbon dioxide are generated, according to Art Hirsch, president of TerraLogic, an engineering consulting service.

It’s unknown how many shipments leave Superfund sites. Company records provide a glimpse of one small part of the Silicon Valley site.

In 2010, at least 12 cross-country shipments of waste were sent from one area at the Silicon Valley site, resulting in [roughly 40 tons of carbon dioxide emissions.](https://www.documentcloud.org/documents/1030313-fairchild-report-2010.html#document/p21/a146423)

It doesn’t end there. Greenhouse gas emissions also are created once the waste gets to the treatment plant.


${carbon}


The Arizona carbon regeneration facility produced the annual emissions of an oil or gas-burning power plant, according to an analysis compiled for the EPA’s Superfund division. It concluded that for every 5 pounds treated, [3.5 pounds of carbon dioxide are produced.](https://www.documentcloud.org/documents/1028803-epa-siemens.html#document/p13/a146233)

In total, the effort of pulling about 900 pounds of contaminants from one area at the Silicon Valley site, shipping it thousands of miles and treating it contributed to more than [4 million pounds of carbon dioxide](https://www.documentcloud.org/documents/1030313-fairchild-report-2010.html#document/p12/a146428) entering the atmosphere in 2010.

That’s roughly equivalent to 12,500 cars idling for a day and a half, said Jack Clayton, president of BlueSkyModel, a climate-change consulting firm.

This could be an acceptable risk, if it were actually making the water cleaner. But it’s not.

Pump and treat works well at first, but its effectiveness eventually peters out.

Since cleanup began in the early 1980s, the pumps at the Silicon Valley site have pulled out more than 100,000 pounds of contaminants from the ground, in some cases reducing chemical concentrations by up to 75 percent. But cleanup has stalled.

A [CIR](http://cironline.org/toxictrail/partner?partnername=Guardian) analysis of the past decade’s worth of data found that cleanup isn’t improving the situation. There are more than 500 wells at the Mountain View site for which monitoring data exists. At the majority of them, chemical concentrations remained stable or, in a small number of cases, increased.

Just trying to remove the chemicals can actually make things worse. Eventually, contaminants embedded in the surrounding soils and bedrock can leach into groundwater as a result of the pumping.

For instance, between 2002 and 2006, roughly 80 percent of the contaminants pulled from the groundwater at the Silicon Valley site came from surrounding soil and rock, a [report](https://www.documentcloud.org/documents/1029451-mew-700years-report.html#document/p27/a146244) by environmental consulting firms Geosyntec and GSI found. If those chemicals had been left alone, bacteria and microbes would have broken them down eventually, experts say.

The same pattern holds for tens of thousands of contaminated groundwater sites across the country, a [2012 study](https://www.documentcloud.org/documents/1029760-nrc-report.html#document/p22/a146250) by the National Research Council found. The conclusion: Available technology is incapable of doing the job at these sites.

“Over the next 30 years, you’re looking at $200 billion to clean these complex sites,” said Michael Kavanaugh, a Geosyntec scientist and chairman of the National Research Council study panel. “I think we have to ask ourselves whether this is a reasonable cost for a process that could take 50, 100 or many hundreds of years to complete.”

In many cases, he said the best thing would be to halt treatment and make sure the contamination is contained.


## Challenging the status quo

The EPA has had concerns for the past two decades about whether pump-and-treat systems work in the long run.

It’s also aware of the unintended consequences of the toxic trail. But the agency doesn’t know how to deal with them, its own documents show.

Of its more than $8 billion annual budget, nearly $1.2 billion goes to the Superfund program. That includes funding for roughly 3,000 full-time jobs.

In a [2012 report,](https://www.documentcloud.org/documents/1026251-epa-green-remediation.html#document/p15/a146207) the EPA issued guidelines to minimize the environmental footprint of Superfund cleanups. The agency now measures the toll a cleanup has on water, energy and air at each site. But agency officials said it was too difficult to get their arms around all the different side effects once the waste leaves.

They don’t take these consequences into consideration when looking at the cleanup’s environmental costs.

The system continues to run on inertia. The EPA is concerned about toxic vapors harming residents and workers. Companies already have invested heavily in the systems in place.

The agency takes a passive approach when it comes to what kind of treatment gets used.  It’s up to the companies to push for change, which is often a long and arduous process.


${oversight}


Some do take that initiative. At the Silicon Valley site, Intel abandoned pump and treat years ago in favor of trying bioremediation, which injects chemical-eating microbes into the ground.

Others prefer sticking with a status quo that complies with the decades-old EPA’s plan.

“They may not wish to rock the boat with something new because it may not work,” said Harris-Bishop, the EPA spokesman. He added that changing the EPA’s cleanup plans “is a long, bureaucratic process.”

Some of the companies cleaning up the Silicon Valley site claim their work continues to reduce contamination.

“The remedy is performing as intended,” stated company officials for Schlumberger Ltd., in its 2012 annual report to the EPA. The company assumed responsibility for the site when it took over Fairchild. Schlumberger officials didn’t respond to requests for comment for this story.

There’s also an active community of residents that continues to put pressure on regulators to clean up sites by any method available.

Soon after Google set up a satellite campus in Mountain View in 2012, toxic vapors from contaminants underground [infiltrated the company’s offices,](http://cironline.org/reports/google-employees-face-health-risks-superfund-sites-toxic-vapors-4291) exposing employees to potentially dangerous levels of TCE.

The vapors are especially harmful to pregnant women. If exposed to low levels of the chemical during a crucial three-week period in their first trimester, women face an increased risk of having a baby born with holes in the heart, an [EPA analysis](http://www.epa.gov/iris/subst/0199.htm) found.

Most of the site doesn’t pose a health risk because contamination levels are so low. But even if treatment stopped at most of the site, there are three hot spots that would likely still need some treatment to protect people above.

Lenny Siegel, the executive director of the Center for Public Environmental Oversight, has organized an effective and vocal group of fellow residents to lobby for cleanup.

“At a recent meeting, I compared the technical challenges of groundwater cleanup to rocket science,” Siegel said. “Then, looking around the room, I noticed that a large fraction of the community representatives actually were rocket scientists.”

People like Siegel encourage the EPA to try alternative treatments that don’t contribute to the toxic trail.  But more than anything, he wants the EPA to focus on the hot spots. He and his allies want cleanup to continue, regardless of the method.

That means the waste will continue to go somewhere else.

Stephen Hill, head of the San Francisco water board’s toxics division, said that shouldn’t be the only answer to solving the problem.

“Ideally, regulators are thinking holistically about how to reduce the waste and make problems go away,” Hill said. “Not just how to move them around like a shell game.”

<p class='gia-smallprint'>Correction: An earlier version of this story misstated the amount of carbon dioxide produced by cleanup at a Silicon Valley site.</p>

<p class='gia-smallprint'>Edited by Andrew Donohue and Mark Katches. Copy edited by Sheela Kamath and Christine Lee.</p>

<p class='gia-smallprint'>This story was produced in a collaboration between [The Center for Investigative Reporting](http://cironline.org/toxictrail/partner?partnername=Guardian) and the Guardian US. Contact the reporters at [srust@cironline.org](mailto:srust@cironline.org) and [mdrange@cironline.org](mailto:mdrange@cironline.org). Contact the Guardian U.S. interactive team at [@GuardianUS](https://twitter.com/guardianus).</p>

<img class='gia-fin' src='<%= assets %>/images/end_logos01.jpg'>
