preview
=======

While developing, you can preview the project in different environments:

localhost/path/to/project/build/dev          # the interactive on a naked index.html file
localhost/path/to/project/build/dev/r2       # on the old website
localhost/path/to/project/build/dev/ng       # on the new website

localhost/path/to/project/build/dev/preview  # generic responsive previewing utility

These previews may not be 100% faithful, but they should help you catch most CSS conflicts etc.
