define( function ( require ) {

	'use strict';

	var app, scaffolding;

	var $, d3;

	$ = require( 'jquery' );
	d3 = require( 'd3' );

	scaffolding = require( 'text!./carbon_scaffolding.html' );

	app = {
		launch: function ( el, url ) {

			setup_css_animations();

			app.el = el;
			app.$el = $(el);
			app.el.innerHTML = scaffolding;

			// var dep = require( 'someDependency' );
			// dep();

			var isMobile = window.innerWidth < 900;

			var paths = {
				reference: "M-13,0v-19.5l-1,8l-1-1v-8l2-2.5h2.5l1.5,1.5l1.5-1.5H-5l2,2.5v8l-1,1l-1-8L-5,0H-8l-1-12l-1,12H-13z M-15-20.5l2,1 M-5-19.5l2-1 M-13-13h8 M-10.5-23v-1.5 M-7.5-24.5v1.5 M-9-30c-1.7,0-3,1.3-3,3s1.3,3,3,3s3-1.3,3-3S-7.3-30-9-30z",
				reactivation: "M-144,0h-230v-435h230V0z M-304-70h-50v50h50V-70z M-164-70h-50v50h50V-70z M-324-395h-30v35h30V-395z M-284-395h-30v35h30V-395z M-244-395h-30v35h30V-395z M-204-395h-30v35h30V-395z M-164-395h-30v35h30V-395z M-324-340h-30v35h30V-340z M-284-340h-30v35h30V-340z M-244-340h-30v35h30V-340z M-204-340h-30v35h30V-340z M-164-340h-30v35h30V-340z M-324-285h-30v35h30V-285z M-284-285h-30v35h30V-285z M-244-285h-30v35h30V-285z M-204-285h-30v35h30V-285z M-164-285h-30v35h30V-285z M-324-230h-30v35h30V-230z M-284-230h-30v35h30V-230z M-244-230h-30v35h30V-230z M-204-230h-30v35h30V-230z M-164-230h-30v35h30V-230z M-324-175h-30v35h30V-175z M-284-175h-30v35h30V-175z M-244-175h-30v35h30V-175z M-204-175h-30v35h30V-175z M-164-175h-30v35h30V-175z M-364-435v25 M-354-435v25 M-344-435v25 M-334-435v25 M-324-435v25 M-314-435v25 M-304-435v25 M-294-435v25 M-284-435v25 M-274-435v25 M-264-435v25 M-254-435v25 M-244-435v25 M-234-435v25 M-224-435v25 M-214-435v25 M-204-435v25 M-194-435v25 M-184-435v25 M-174-435v25 M-164-435v25 M-154-435v25 M-374-410.5h230 M-234-75h-50v65h50V-75z M-259-75v65 M-284-75h50v-10h-50V-75z M-289,0h60v-10h-60V0z M-144-120h-230v20h230V-120z",
				transport: "M-254-25h-30V0h30V-25z M-254-25l5-30 M-259-25l5-30 M-279-25l-5-30 M-289-55l5,30 M-234-85h-70l15,30h40L-234-85z M-304-85h35 M-269-85h35c0,0,100-94.2,100-175c0-100-135-100-135-100s-135,0-135,100c0,80.8,100,175,100,175H-269 M-239.4-85c0,0,70.4-94.2,70.4-175c0-100-95-100-95-100 M-244.4-85c0,0,70.4-94.2,70.4-175c0-100-95-100-95-100 M-257.3-85c0,0,33.3-94.2,33.3-175c0-100-45-100-45-100 M-252.3-85c0,0,33.3-94.2,33.3-175c0-100-45-100-45-100 M-269-360c0,0-45,0-45,100c0,80.8,33.3,175,33.3,175 M-274-360c0,0-45,0-45,100c0,80.8,33.3,175,33.3,175 M-269-360c0,0-95,0-95,100c0,80.8,70.4,175,70.4,175 M-274-360c0,0-95,0-95,100c0,80.8,70.4,175,70.4,175 M-271.5-85v-275 M-266.5-85v-275 M-284-15h30 M-254-10h-30 M-284-5h30 M-294-85l5,30 M-244-85l-5,30 M-284-55v-30 M-254-85v30 M-264-70h-10v15h10V-70z M-264-70v-10l-5,5l-5-5v10H-264z",
				pumping: "M-664-565h-390v-40h390V-565z M-714-695h-290v40h290V-695z M-1014-605h310v-50h-309.9L-1014-605z M-684-565h-350v270h350V-565z M-1034-295l-35,50h420l-35-50H-1034z M-494-75h-730V0h730V-75z M-1204-75h690v-60h-690V-75z M-629-195h-460v60h460V-195z M-1069-195h420v-50h-420V-195z M-839-1250l5-30l5-5l25-10h-25l-5-10l30-20l-35,15l-10-5l15-45l-25,40h-10l-25-40l15,45l-10,5l-35-15l30,20.5l-5,9.5h-25l25,10l5,5v15l5,10l-10,25l-35-73l-30-117l-10-10v-10l20-20v-25h-20v-10l10-15h-10l-10-15l-10,15v25h-20v25l20,20v10l-10,10l17,60h8l20,105l-14.5,25l4.5,30l10,5v10l10,15l5,35l10,15l-20,40v40l10,15.5l-4.5,34.5l4.5,30l5.5,32l-0.5,38l11.5,41l13.5,69l-35,85h205l-20-50l-10-280l15,40h15l25-50l5-75l10-20l-30-24.5l-15,29.5l-18-15l-37-65h-20 M-864-1280l-5,15v5h10 M-749-1125l-25,55v45l15,4.5l45-89.5 M-954-1460h-60 M-929-1240l-30,45 M-824-1205l-110,85 M-819-1205l-125,180 M-814-1205l-125,315 M-914-780l75-50l-30-20 M-794-895l-15,20l15,5l9.5,65l15.5,5l-10,20l15,35 M-794-895l5-120l-13-20l18-14.5l-45-65.5v35l-30,35l-10,195L-794-895z M-809-1205l-20,90 M-804-1205l30,135 M-929-1200l-20,30 M-889-1255l5,10l15,10h10l15-10 M-859-1280l10,5 M-869-1280l-10,5 M-869-1250h10 M-839-1250l15,40h10l-10-50l-5-25 M-834-1280l-10-15l-10-5h-20l-10,5l-10,15 M-844-1245l5,40h15 M-899-1230l10,25l5-40 M-844-1245l5-5"
			};

			//////////

			var positions = [
				{
					reference:		[  335, 0, 7.50,    0 ],
					reactivation:	[ -999, 0, 0.50,    0 ],
					transport:		[ -999, 0, 0.50,    0 ],
					pumping:		[ -999, 0, 0.15,    0 ]
				},
				{
					reference:		[  335, 0, 7.50, 2000 ],
					reactivation:	[ -999, 0, 0.50, 2000 ],
					transport:		[ -999, 0, 0.25, 2000 ],
					pumping:		[ -999, 0, 0.15, 2000 ]
				},
				{
					reference:		[  750, 0, 0.50, 2000 ],
					reactivation:	[  250, 0, 0.50, 2000 ],
					transport:		[ -999, 0, 0.25, 2000 ],
					pumping:		[ -999, 0, 0.15, 2000 ]
				},
				{
					reference:		[  700, 0, 0.25, 2000 ],
					reactivation:	[  500, 0, 0.25, 2000 ],
					transport:		[  225, 0, 0.25, 2000 ],
					pumping:		[ -999, 0, 0.15, 2000 ]
				},
				{
					reference:		[  925, 0, 0.15, 2000 ],
					reactivation:	[  800, 0, 0.15, 2000 ],
					transport:		[  625, 0, 0.15, 2000 ],
					pumping:		[  200, 0, 0.15, 2000 ]
				}
			];

			var stages = [
				{	name: "reference",
					cube: 2,
				},
				{	name: "pumping",
					cube: 330,
				},
				{	name: "reactivation",
					cube: 96,
				},
				{	name: "transport",
					cube: 90,
				}
			];

			var annotationPositions = [
				[ 450, 160, "One pound of carbon emissions", "2'1", "fills a 25-inch cube. The average car releases slightly less for every mile driven." ],
				[ 450, 160, "One pound of carbon emissions", "2'1", "fills a 25-inch cube. The average car releases slightly less for every mile driven." ],
				[ 460,  10, "Burning the carbon", "96'", "creates enough emissions to fill a 96-foot cube, taller than a six-story building." ],
				[ 375, 135, "Transporting the spent carbon", "90'", "produces more emissions than are created by driving around the world three-and-a-half times." ],
				[ 425,   0, "Pumping the contaminated water ", "330'", "through the filters creates the most emissions: enough to fill a 330-foot cube, taller than the Statue of Liberty." ]
			];

			var arrowPositions = [
				{ start:{ "x": 440, "y": 200 }, end: { "x": 390, "y": 250 } },
				{ start:{ "x": 440, "y": 200 }, end: { "x": 390, "y": 250 } },
				{ start:{ "x": 450, "y":  50 }, end: { "x": 400, "y": 100 } },
				{ start:{ "x": 365, "y": 175 }, end: { "x": 315, "y": 225 } },
				{ start:{ "x": 415, "y":  40 }, end: { "x": 365, "y":  90 } },
			];

			//////////

			var width  = 940;
			var height = 600;

			var svg = d3.select("#illustration-container")
				.append("svg")
					.attr("width", 940)
					.attr("height", 365);

			var baseline = svg.append("line")
				.attr("x1", 0)
				.attr("y1", 360)
				.attr("x2", width)
				.attr("y2", 360);

			var index = counter(1, positions.length);

			// Draw cubes

			var annotation = d3.select("#annotation");

			// Draw references

			var references = (function (elements) {
				return elements.map( function (element) {
					var group = svg.append("g").attr("class", element.name);
					var elements = group.append("path").attr("d", paths[element.name]);
					return group;
				});
			})(stages);

			//////////

			function transformReferences (elements, index) {
				elements.forEach ( function (element) {
					var name = element.attr("class");
					var transformation = positions[index][name];
					transformReference( element, transformation[0],  transformation[1],  transformation[2], transformation[3] );
				});
			}

			function transformReference (element, x, y, scale, duration) {
				var getDuration = function getDuration (el) {
					return parseInt(d3.select(this).attr("stroke-width")) > (2 / scale);
				};

				element
					.transition().duration(duration)
						.attr("transform", function () {
							return "translate(" + x + "," + y + 360 + ")" + "scale(" + scale + ")";
						});

				element.select("path")
					.transition().duration( duration / 2 )
					.delay( function () { return getDuration.call(this) ? duration * 0 : duration * 0.5; })
					.attr("stroke-width", function () {
						return 2 / scale + "px";
					});
			}

			function counter (min, max) {
				var index = min;
				var previousIndex = min;

				function getIndex () {
					return index;
				}

				function getPrevious () {
					return previousIndex;
				}

				function next() {
					previousIndex = index;
					return index < max - 1 ? ++index : index;
				}

				function previous() {
					previousIndex = index;
					return index > min ? --index : index;
				}

				return {
					getIndex: getIndex,
					getPrevious: getPrevious,
					next: next,
					previous: previous
				};
			}

			//////////

			function transformAnnotation (element, index) {
				element
					.transition().duration(1000)
						.style({
							width: "0px",
						})
					.each("end", moveElements );

				function moveElements () {

					// <div id="annotation-title"></div>
					// <div id="annotation-cube"></div>
					// <div id="annotation-text"></div>

					element.select("#annotation-title")
						.html( function () {
							return annotationPositions[index][2];
						});

					/*
					element.select("#annotation-cube")
						.text(annotationPositions[index][3]);
					*/

					element.select("#annotation-text")
						.html(annotationPositions[index][4]);

					element
						.style({
							left: annotationPositions[index][0] + "px",
							top: annotationPositions[index][1] + "px"
						})
					.transition().duration(1000)
						.style({
							width: "250px",
						});
				}
			}

			// add marker arrow

			(function () {
				svg.append("defs").append("marker")
					.attr("id", "arrowhead")
					.attr("viewBox", "0 0 10 10")
					.attr("refX", "1")
					.attr("refY", "5")
					.attr("markerWidth", 6)
					.attr("markerHeight", 6)
					.attr("orient", "auto")
					.append("path")
						.attr("d", "M 0 0 L 10 5 L 0 10 z");
			})();

			function drawArrow (index) {

				var start = arrowPositions[index].start;
				var end   = arrowPositions[index].end;
				var mid   = {
					x: ( (end.x - start.x) / 2 ) + start.x,
					y: ( (end.y - start.y) / 2 ) + start.y
				};

				// The 10 should be converted to a relative unit

				/*
				var target = {
					x: ( (mid.x - start.x) / 2 ) + start.x + 10,
					y: ( (mid.y - end.y) / 2 ) + end.y + 10
				}
				*/

				var target = {
					x: ( ( mid.x - end.x   ) / 2 ) + end.x - 10,
					y: ( ( mid.y - start.y ) / 2 ) + start.y - 10
				};

				var quad = function (start, end) {
					return "M" + start.x + "," + start.y + " Q " + target.x + " " + target.y + " " +  end.x + " " + end.y;
				};

				var tweenDash = function (dashPosition) {
						var	l = this.getTotalLength();
						var	i = d3.interpolateString(0 + "," + l, l + "," + l);
						return function(t) { return i(t); };
				};

				svg.selectAll(".arrow").remove();

				svg.append("path")
					.attr("class", "arrow")
					.attr("d", quad(start, end))
					.attr("stroke-dasharray", function() { return 0 + "," + this.getTotalLength(); })
					.transition()
						.delay(1500)
						.duration(250)
						.attr("stroke-width", 2)
						.attrTween("stroke-dasharray", function(){
							return tweenDash.call(this);
						})
					.each("end", function () {
						d3.select(this).attr("marker-end", "url(#arrowhead)");
					});
			}

			function transformCubes (index) {
				stages.forEach( function (el) {
					var element = "#gas-" + el.name;
					var background;
					d3.select(element)
						.transition()
						.duration(2000)
						.style({
							"left": positions[index][el.name][0]+ "px",
							"width": Math.floor(positions[index][el.name][2] * ( el.cube + ( el.cube / 5 * 2) ) * 5) + "px",
							"height": Math.floor(positions[index][el.name][2] * ( el.cube + ( el.cube / 5 * 2) ) * 5) + "px",
						});

						// ( el.cube + (el.cube / 5 * 2) )

					// background = "url(" + url + "/<%= versionDir %>/files/carbon/images/gas-large.jpg)";

					// console.log( '%cspinning cube background: ' + background, 'color: green; font-size: 1.5em' );

					// d3.select(element).select(".filmstrip").style({
					// 	"background-image": background
					// });
				});
			}

			///////////////

			function updateTable (index) {
				var description = d3.select(".description");
				var lookup = [ "reactivation", "transport", "pumping" ];

				if(index <= 1) {
					description
						.transition().duration(1000)
						.style({
							"opacity": "0"
						});
				}
				else {
					description
						.transition().duration(1000)
						.style({
							"opacity": "1"
						});
				}

				$(".description").children(".carbon-equivalent-row").removeClass("open");

				for (var i = 0; i < index - 1; i++ ) {
					var selection = "#" + lookup[i];
					$(".description").children(selection).addClass("open");
				}
			}



			function updateTotals (index) {
				var pounds = [ 98000, 79400, 3960000 ];
				var tmiles = 0;
				var tpounds = 0;

				for (var i = index - 2; i >= 0; --i) {
					tpounds = tpounds + pounds[ i ];
				}

				$(".carbon-equivalent-row-total").children(".miles").text( commify(Math.floor(tpounds * 0.914)) );
				$(".carbon-equivalent-row-total").children(".carbon").text( commify(tpounds) );
			}

			////////////////////

			// setup_css_animations();

			function setup_css_animations() {
				var prefixes = [ '-webkit-', '-moz-', '-o-', '' ];
				var keyframes_css = '';
				var filmstrip_steps = 40;
				var background_url = url + "/<%= versionDir %>/files/carbon/images/gas-large.jpg";
				var background_image_css = '#gia-carbon .filmstrip { background-image: ' + "url(" + background_url + "); }";

				console.log( '%cspinning cube background: ' + background_url, 'color: green; font-size: 1.5em' );

				for (var i = 0; i < prefixes.length; i++) {

					var filmstrip = '';
					for (var f = 0; f < filmstrip_steps; f++) {
						var current_pct = f * (100/filmstrip_steps);
						filmstrip += current_pct + '% {background-position:0 -' + (f * 100) + '%;' + prefixes[i] + 'animation-timing-function:steps(1);}';
					}
					keyframes_css += '@' + prefixes[i] + 'keyframes filmstrip {' + filmstrip + '}';
				}

				var s = document.createElement('style');
				s.innerHTML = background_image_css + keyframes_css;
				$('head').append(s);
			}

			////////////////////

			// Utilities //

			function commify(x) {
				return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			}

			// Listeners //

			app.$el.find("#gia-carbon-next").on("click", function(){
				if ( !$(this).hasClass( "disabled-button" )) {
					updateState( index.next() );
				}
			});

			app.$el.find("#gia-carbon-previous").on("click", function(){
				if ( !$(this).hasClass( "disabled-button" )) {
					updateState( index.previous() );
				}
			});

			function updateState (indexNumber) {
				$("#gia-carbon-next").removeClass("disabled-button");
				$("#gia-carbon-previous").removeClass("disabled-button");

				if (indexNumber === 4) {
					$("#gia-carbon-next").addClass("disabled-button");
				}
				else if ( indexNumber === 0 || indexNumber === 1 ) {
					$("#gia-carbon-previous").addClass("disabled-button");
				}

				updateTable( indexNumber );
				updateTotals( indexNumber );
				drawArrow( indexNumber );
				transformAnnotation( d3.select("#gia-oversight-annotation"), indexNumber);
				transformCubes( indexNumber );
				transformReferences( references, indexNumber);
			}

			if( isMobile ){
				positions[0].reference = [ 0, 0, 7.5,    0 ];
				annotationPositions[0] = [ 125, 0, "One pound <br> of carbon emissions", "2'1", "fills a 25-inch cube. The average car releases slightly less for every mile driven." ];
				arrowPositions[0] = { start:{ "x": 140, "y": 200 }, end: { "x": 190, "y": 250 } };

				updateTable( 4 );
				updateTotals( 4 );
				// drawArrow( 0 );
				transformAnnotation( d3.select("#gia-oversight-annotation"), 0);
				transformCubes( 0 );
			}
			else {
				updateState(0);
			}
		}
	};

	window.app = app; // useful for debugging!

	return app;

});
