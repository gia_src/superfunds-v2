define( function ( require ) {

    var d3 = require( 'd3' );
    var loadJSONP = require( 'prol_loadJSONP' );

    var app = {
        launch: function ( el, baseUrl ) {

            el.innerHTML = require( "text!proliferation.html" );

            var mewsites = [ "CAD095989778", "CAD009205097", "CAD061620217" ];

            var w = 700;
            var h = 350;
            var padding = 25;

            var svg = d3.select("#proliferation-graphic")
                .append("svg")
                    .attr("width", w)
                    .attr("height", h + padding * 2);

            var annotations = d3.select("#proliferation-graphic-annotations")
                .append("svg")
                    .attr("width", 940)
                    .attr("height", h + padding * 2)
                .append("g")
                    .attr("class", "annotations")
                    .attr("transform", "translate(0," + ( padding * 2 ) + ")");

            var axis = svg.append("g")
                .attr("class", "axis")
                .attr("transform", "translate(0," + ( 400 ) + ")");

            var sites = svg.append("g")
                .attr("class","sites")
                .attr("transform", "translate(0," + ( padding * 2 ) + ")");

            loadJSONP(baseUrl + "/<%= versionDir %>/files/proliferation/milestones.js", 'loadProliferationSites', function ( csv ) {
                var result = d3.csv.parse( csv );

                result = result.map( function ( record ) {
                    record.added = convertToDate( record.added );
                    record.deleted = convertToDate( record.deleted );
                    record.mew = mewsites.indexOf( record.id ) !== -1;

                    return record;
                });

                drawAnnotationLines();
                drawAxis();
                drawSites(result);
                drawPoints(result);
            }, function ( err ) {
                throw err;
            });

            // d3.csv(baseUrl + "/<%= versionDir %>/files/proliferation/milestones.csv",
            //     function ( result ) {
            //         result.added = convertToDate( result.added );
            //         result.deleted = convertToDate( result.deleted );
            //         result.mew = mewsites.indexOf( result.id ) !== -1;
            //         return result;
            //     },
            //     function ( error, rows ) {
            //         drawAnnotationLines();
            //         drawAxis();
            //         drawSites(rows);
            //     });

            function drawSites ( data ) {
                sites.selectAll("line")
                    .data(data)
                    .enter()
                    .append("line")
                    .attr("class", function ( d, i ) {
                        if ( d.mew ) {
                            return "selected line";
                        }
                        else {
                            return "line";
                        }
                    })
                    .attr("x1", function ( d,i ) { return xScale( d.added ); })
                    .attr("y1", function ( d,i ) { return yScale(i); } )
                    .attr("x2", function ( d,i ) { return xScale( d.deleted ); })
                    .attr("y2", function ( d,i ) { return yScale(i); } );

                sites.selectAll("line").sort(function ( a, b ) {
                    if ( a.mew ) {
                        return 1;
                    }
                    else {
                        return -1;
                    }
                });
            }

            function drawPoints ( data ) {
                sites.selectAll("circle")
                    .data(data)
                    .enter()
                    .append("circle")
                    .attr("class", function ( d ) {
                        if ( d.deleted < new Date( 2014, 2, 10 ) ) {
                            return "gia-deleted-dot";
                        }
                        else {
                            return "gia-deleted-dot-hidden";
                        }
                    })
                    .attr("cx", function ( d,i ) { return xScale( d.deleted ); })
                    .attr("cy", function ( d,i ) { return yScale(i); } )
                    .attr("r", "1.5");
            }

            function drawAnnotationLines ( ) {

                var annotationLines = [
                    // [ { "x": 0,   "y": 165 },  { "x": 245,  "y": 165 }, { "x": 245,  "y": yScale(  480 ) }, { "x": 405,  "y": yScale(  480 ) } ],
                    // [ { "x": 0,   "y": 165 },  { "x": 245,  "y": 165 }, { "x": 245,  "y": yScale( 1012 ) }, { "x": 310,  "y": yScale( 1012 ) } ],
                    // [ { "x": 0,   "y": 165 },  { "x": 245,  "y": 165 }, { "x": 245,  "y": yScale( 1054 ) }, { "x": 310,  "y": yScale( 1054 ) } ],

                    [ { "x": 0,   "y": 310 },  { "x": 245,  "y": 310 }, { "x": 245,  "y": yScale( 1283 ) }, { "x": 245,  "y": yScale( 1283 ) } ],
                    [ { "x": 0,   "y": 310 },  { "x": 245,  "y": 310 }, { "x": 245,  "y": yScale( 1688 ) }, { "x": 245,  "y": yScale( 1688 ) } ],
                ];

                annotationLines.forEach( function (el) {
                    annotations
                        .append( "path" )
                        .attr("d", lineFunction( el ) );
                });
            }

            function drawAxis () {
                axis
                    .call(xAxis);
            }

            function convertToDate ( date ) {
                var format = d3.time.format("%Y-%m-%d");
                return format.parse( date );
            }

            var startDate = new Date( 1983, 1, 1 );
            var endDate = new Date( 2014, 3, 10 );

            var xScale = d3.time.scale()
                .domain([ startDate, endDate ])
                .range([ 0, 700 ]);

            var yScale = d3.scale.linear()
                .domain([ 0, 1689 ])
                .range([ 0, 350 ]);

            var xAxis = d3.svg.axis()
                .scale(xScale)
                .orient("top")
                .innerTickSize([ 375 ]);

            var lineFunction = d3.svg.line()
                .x(function(d) { return d.x; })
                .y(function(d) { return d.y; })
                .interpolate("step-after");

        }
    };

    return app;

});
