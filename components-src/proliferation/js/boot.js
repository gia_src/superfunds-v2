(function () {

    var config, localRequire, launch;

    // feature detection
    if ( window.innerWidth < 800 ) {
        return;
    }

    config = {
        context: 'proliferation',
        baseUrl: '<%= components %>/proliferation/',

        paths: {
            d3: '<%= assets %>/lib/d3.min'
        }
    };

    launch = function ( proliferation ) {
        var target = document.getElementById( 'gia-proliferation' ),
            baseUrl = '<%= projectUrl %>';

        proliferation.launch( target, baseUrl, '<%= assets %>' );
    };

    if ( window.GIA_LOADER_IS_REQUIRE ) {
        localRequire = require.config( config );
        localRequire([ 'proliferation' ], launch );
    } else {
        require( config, [ 'proliferation' ], launch );
    }

}());
