define( function ( require ) {

	'use strict';

	var app, d3, $, L, scaffolding, loadJSONP;

	d3 = require( 'd3' );
	$ = require( 'jquery' );
	L = require( 'lib/leaflet.mapbox' );
	scaffolding = require( 'text!./scaffolding.html' );
	loadJSONP = require( 'map_loadJSONP' );

	app = {
		launch: function ( el, baseUrl ) {
			var isMobile;

			app.el = el;
			app.$el = $(el);
			app.baseUrl = baseUrl;

			app.el.innerHTML = scaffolding;

			$(document).ready(function(){

				var $window = $(window);

				function toRad (deg) {
					return deg * Math.PI / 180;
				}

				function toDeg (rad) {
					return rad * 180 / Math.PI;
				}

				function getUniques(value, index, self) {
					return self.indexOf(value) === index;
				}

				var sites;
				var bounds = [];
				var contaminates;
				var zips;
				var selectedSites = [];
				var selectedSiteIds = [];
				var selectedMedias = ["groundwater"];
				var selectedContaminants = ["VOCs"];

				var location = false;
				var popupSite;

				var line;

				var screen = initScreen();

				//////////

				function initScreen() {
					var width = $window.width();

					var wasMobile = isMobile;

					isMobile = app.el.offsetWidth < 800;

					if ( wasMobile !== isMobile ) {
						if ( isMobile ) {
							app.$el.addClass( "gia-mobile" );
						} else {
							app.$el.removeClass( "gia-mobile" );
						}
					}

					return {
						screenWidth: width,
						mobile: width < 800
					};
				}

				var map = L.mapbox.map('map', 'gia.hccg9p1l', {
					maxZoom: 10,
					minZoom: 2,
					doubleClickZoom: false,
					scrollWheelZoom: false,
					inertia: false
				})
					.setView([40, -95], 4);

				$window
					.on("resize", function(){
							screen = initScreen();
							moveMobile();
							if (popupSite) {
								moveMarker(popupSite.id);
							}
						});

				// templating

				var hover = d3.select("#map-wrapper").append("div")
					.attr("class", "site-details dropshadow")
					.attr("id", "site-detail");

				hover.append("div")
						.attr("class", "details-name");

				hover.append("div")
						.attr("class", "details-place");

				var population = hover.append("div")
						.attr("class", "details-row");

				population.append("div")
						.attr("class", "detail-label")
						.text("POP WITHIN 1 MILE");
				population.append("span")
					.attr("class", "details-population");

				var exposure = hover.append("div")
						.attr("class", "details-row");

				exposure.append("div")
					.attr("class", "detail-label")
					.text("HUMAN EXPOSURE");

				exposure.append("span")
					.attr("class", "details-human-exposure");

				var pump = hover.append("div")
						.attr("class", "details-row");
					pump.append("div")
						.attr("class", "detail-label")
						.text("PUMP AND TREAT");
					pump.append("span")
						.attr("class", "details-pump");


				var svg = d3.select(map.getPanes().overlayPane).append("svg"),
					g = svg.append("g").attr("class", "leaflet-zoom-hide"),
					g2 = svg.append("g").attr("class", "leaflet-zoom-hide");

				///////////////

				function lineFunction(interpoloation){
					return d3.svg.line()
						.x(function(d) { return projectPoint(d[1], 0).x; })
						.y(function(d) { return projectPoint(0, d[0]).y; })
						.interpolate(interpoloation);
				}

				// $.when(
				// 	$.getJSON(baseUrl + "/<%= versionDir %>/files/map/sites.json")
				// 	).done(function(sitesData){
				// 		sites = sitesData;
				// 		getContaminates();
				// });

				loadJSONP( baseUrl + "/<%= versionDir %>/files/map/sites.js", 'loadMapSites', function ( s ) {
					sites = s;
					getContaminates();
				});

				function getContaminates(){

					if( !screen.mobile ){
						loadJSONP( baseUrl + "/<%= versionDir %>/files/map/crunched.js", 'loadMapCSV', function ( data ) {
							var contents = d3.csv.parse( data );
							contaminates = contents;
							contaminates = contaminates.map(function(el){
								el.id = parseInt(el.id);
								return el;
							});
							init();
						}, function ( err ) {
							throw err;
						});
					}
					else {
						init();
					}

					// $.get(baseUrl + "/<%= versionDir %>/files/map/crunched.csv" ).then( function ( csv ) {
					// 	console.log( JSON.stringify( csv ) );
					// });

					// d3.csv(baseUrl + "/<%= versionDir %>/files/map/crunched.csv", function(contents) {
					// 	contaminates = contents;
					// 	contaminates = contaminates.map(function(el){
					// 		el.id = parseInt(el.id);
					// 		return el;
					// 	});
					// 	getZIPS();
					// 	// init();
					// });
				}

				function getZIPS(){
					d3.csv(baseUrl + "/<%= versionDir %>/files/map/zips.csv", function(contents) {
						zips = contents;

						//	contaminates = contaminates.map(function(el){
						//		el.id = parseInt(el.id);
						//		return el;
						//	});

						init();
					});
				}

				//////////

				function init(){
					sites = sites.map(function(el){
						el.selected = false;
						return el;
					});

					getBounds();

					g.selectAll("circle")
						.data(sites, function(d) { return d.id; } )
					.enter().append("circle")
						.attr("cx", function(d) { return projectPoint(d.lng, 0).x; } )
						.attr("cy", function(d) { return projectPoint(0, d.lat).y; } )
						.attr("r", function() { return map.getZoom() * 0.65; } )
					.on( "mouseover", function(d){
						d3.select(this)
							.transition()
								.attr("r", function(){ return map.getZoom() * 2; });
								// .style("fill", "#000000");
						updateMarker(d);
						moveMarker(d.id);
					})
					.on( "mouseout", function(){
						d3.select(this)
							.transition()
								.attr("r", function(){ return map.getZoom() * 0.65; })
								.style("fill", function(d){
									return d.selected ? "#003366" : "#CCCCCC";
								});
					})
					.on( "touchstart", function(d){
						d3.select(this)
							.transition()
								.attr("r", function(){ return map.getZoom() * 2; })
							.transition().delay(500)
								.attr("r", function(){ return map.getZoom() * 0.65; });
								// .style("fill", "#000000");
						updateMarker(d);
						// moveMarker(d.id);
					});

					map
						.on("viewreset", viewReset)
						.on("zoomstart", hideDetails)
						.on("zoomend", checkZoom)
						.on("moveend", moveEnd );

					if( screen.mobile ){
						console.log("locate me!");
						map.locate({});
					}

					map
						.on("locationfound", function (e) {
							location = e;
							moveToMobilePoint();
						})
						.on("locationerror", function (e) { });

					//$("#address").focus();

					getSelectedFilters();

					// CAD095989778

					var startingId = 901680;
					var startObject = sites.filter(function(el){
							return el.id === startingId;
						})[0];

					updateMarker(startObject);
					moveMarker(startingId);

					if (screen.mobile) {
						// getMatchingSites([],[]);
						map.setView([startObject.lat, startObject.lng], 6);
					}
					else {
						getMatchingSites(["VOCs"],["GW"]);
					}

					moveTileSites();
					colorTileSites();
					// reset();
				}

				function moveEnd () {
					// console.log("Move end!");
				}

				function checkZoom () {
					var bounds = map.getBounds();

					if ( bounds._southWest.lat < popupSite.lat && bounds._southWest.lng < popupSite.lng ) {
						if ( bounds._northEast.lat > popupSite.lat && bounds._northEast.lng > popupSite.lng ) {
							showDetails();
						}
					}
				}

				function viewReset () {
					console.log("view reset");
					reset();
					moveTileSites();
				}

				function resetMapMove () {
					/*
					if (!screen.mobile) {
						var offset = L.DomUtil.getPosition(map.getPanes().mapPane);
						var object = $("#site-detail");
						console.log( "offset: " + JSON.stringify( offset ));
						var currentX = parseInt( object.css("left"), 10);
						var currentY = parseInt( object.css("top"), 10);
						console.log( "currentX: " + JSON.stringify( currentX ));
						console.log( "currentY: " + JSON.stringify( currentY ));
						object.css("left", ( offset.x - currentX ) + "px" );
						object.css("top", ( offset.y - currentY ) + "px" );
						console.log();
					}
					*/
				}

				function moveToMobilePoint() {
					addDistance(location.latitude, location.longitude);
				}

				function getSiteObject(id){
					return sites.filter(function(el){
						return el.id === id;
					})[0];
				}

				function moveMobile(){
					/*
					if( screen.mobile ) {
						var mapWidth = $("#map").width();
						$(".site-details").css({
							display: "block",
							left: 0 - map._mapPane._leaflet_pos.x + "px",
							top: 175 - map._mapPane._leaflet_pos.y + "px",
							width: mapWidth + "px"
						});
					}
					else {
						$(".site-details").css({
							width: ""
						});
					}
					*/
				}

				function generateStars(number){
					if(number && number > 0) {
						return repeat("&#9733; ", number);
						// return "";
					}
					else {
						return "";
					}
				}

				function repeat (string, number) {
					return new Array(parseInt(number) + 1).join(string);
				}

				function hideDetails(){
					if ( !screen.mobile ) {
						$("#site-detail").css("display", "none");
					}
				}

				function showDetails(){
					$("#site-detail").css("display", "block");
					// this is an odd place for this
					if(popupSite && popupSite.id){
						moveMarker(popupSite.id);
					}
				}

				function getBounds() {

					var longso = {
						min: -176.6525,
						max: -64.885
					};
					var latso = {
						max: 13.43916,
						min: 64.823
					};


					/*
					var lngs = sites.map(function(el){
						var projectedPoint = projectPoint(el.lng, 0);
						// console.log(projectedPoint.x);
						return projectedPoint.x;
					});

					var lats = sites.map(function(el){
						var projectedPoint = projectPoint(0, el.lat);
						return projectedPoint.y;
					});


					var lngss = sites.map(function(el){
						return el.lng;
					});

					var latss = sites.map(function(el){
						return el.lat;
					});
					*/


					// console.log("lngs", d3.min(lngss), d3.max(lngss));
					// console.log("lats", d3.min(latss), d3.max(latss));

					// bounds[0] = [ d3.min(lngs) - 150, d3.min(lats) - 150 ];
					// bounds[1] = [ d3.max(lngs) + 150, d3.max(lats) + 150 ];

					// var bounds2 = [];

					bounds[0] = [ projectPoint( longso.min, 0 ).x - 150, projectPoint( 0, latso.min ).y - 150 ];
					bounds[1] = [ projectPoint( longso.max, 0 ).x + 150, projectPoint( 0, latso.max ).y + 150 ];

					// console.log( bounds );
					// console.log( bounds2 );

					return;
				}

				function projectPoint(x, y) {
					return map.latLngToLayerPoint(new L.LatLng( y, x ));
				}

				function drawTileSites() {

					console.log("drawTile-Sites");

					var zoom = map.getZoom();

					g.selectAll("circle")
						.attr("cx", function(d) { var projectedPoint = projectPoint(d.lng, 0); return projectedPoint.x; } )
						.attr("cy", function(d) { var projectedPoint = projectPoint(0, d.lat); return projectedPoint.y; } )
					.transition()
						.attr("r", zoom * 0.65)
						.style("fill", function(d){
							return d.selected ? "#005689" : "#CCCCCC";
						});
				}

				function moveTileSites () {

					var zoom = map.getZoom();

					g.selectAll("circle")
						.attr("cx", function(d) { var projectedPoint = projectPoint(d.lng, 0); return projectedPoint.x; } )
						.attr("cy", function(d) { var projectedPoint = projectPoint(0, d.lat); return projectedPoint.y; } )
					.transition()
						.attr("r", zoom * 0.65);

					return;

				}

				function colorTileSites () {
					g.selectAll("circle")
						.transition()
							.style("fill", function(d){
								if  ( screen.mobile ) {
									return"#005689";
								}
								else {
									return d.selected ? "#005689" : "#CCCCCC";
								}
							});
				}

				function highlightedSelected(){
					sites = sites.map(function(el){
						el.selected = selectedSiteIds.indexOf(el.id) !== - 1 ? true : false;
						return el;
					});

					selectedSites = sites.filter(function(el){
						return el.selected;
					});

					colorTileSites();

					return;

					// reset();
				}

				function reset() {

					console.log("reset");

					getBounds();

					var topLeft = bounds[0];
					var bottomRight = bounds[1];

					svg
						.attr("width", bottomRight[0] - topLeft[0])
						.attr("height", bottomRight[1] - topLeft[1])
						.style("left", topLeft[0] + "px")
						.style("top", topLeft[1] + "px");

					g
						.attr("transform", "translate(" + -topLeft[0] + "," + -topLeft[1] + ")");
					g2
						.attr("transform", "translate(" + -topLeft[0] + "," + -topLeft[1] + ")");

					if ( !screen.mobile ) {
						g.selectAll("circle").sort(function (a,b) {
							return a.selected > b.selected ? 1 : -1;
						});
					}

					g2.selectAll("path")
						.attr("d", function(d){
							// var line = map.getZoom() < 8 ? lineFunction("basis-closed") : lineFunction("basis");
							if (map.getZoom() < 9) {
								return "M0,0";
							}
							else {
								return line(d);
							}
						});

					g2.selectAll("text")
						.attr("x", function(d) { return projectPoint(d[1], 0).x; })
						.attr("y", function(d) { return projectPoint(0, d[0]).y; })
						.attr("font-size", function() { var zoom = map.getZoom(); if (zoom < 9) { return 0; } else { return zoom * 1.75; } });

					return;
					// drawTileSites();
				}

				function getAddress ( url ) {
					if ( window.XDomainRequest ) {
						var xdr = new XDomainRequest();
						xdr.onerror = function () { };
						xdr.ontimeout = function () {

						};
						xdr.onprogress = function () { };
						xdr.onload = function() {
							$("#loader").removeClass("loading");
							var result = JSON.parse( xdr.responseText );
							if ( result[0] ) {
								var lat = result[0].lat;
								var lng = result[0].lon;
								setView(lat, lng);
								drawBuffer(lat, lng, 10);
								addDistance(lat, lng);
							}
						};
						xdr.timeout = 5000;
						xdr.open("get", url);
						xdr.send();
					}
					else {
						var request = $.ajax({
							crossDomain: true,
							dataType: "json",
							timeout: 5000,
							type: "GET",
							url: url
						});

						request.always( function() { });

						request.done( function(result) {
							$("#loader").removeClass("loading");

							if ( result[0] ) {
								var lat = result[0].lat;
								var lng = result[0].lon;
								setView(lat, lng);
								drawBuffer(lat, lng, 10);
								addDistance(lat, lng);
							}
							else {
								// tryZip();
							}
						});

						request.fail( function(err) {
							// tryZip();
						});
					}
				}

				function createRequest(address){
					var baseUrl = "http://nominatim.openstreetmap.org/search?q=";
					// var baseUrl = "http://nominatim.openstreetmap.org/search";
					if(address && address !== "not found") {
						address = address.replace(" ", "+");
						var url = baseUrl + address + "&countrycodes=us&format=json";
						$("#loader").addClass("loading");
						getAddress(url);
					}
				}

				function tryZip () {
					console.log("trying zip");

					var zip = $("#address").val();
					var zipResult = lookupZip(zip);

					if ( zipResult[0] ) {
						var lat = zipResult[0].lat;
						var lng = zipResult[0].lng;
						setView(lat, lng);
						drawBuffer(lat, lng, 10);
						addDistance(lat, lng);
						// getSelectedFilters();
					}
					else {
						$("#address").val("not found");
					}
				}

				function lookupZip (zip) {
					return zips.filter( function (el) {
						return zip === el.id;
					});
				}

				function addDistance(lat, lng) {
					sites.forEach(function(el) {
						el.distance = getDistance(lat, lng, el.lat, el.lng);
					});

					getClosest();

					return;
				}

				function getClosest() {

					selectedSites.sort(function(a, b) {
						return a.distance - b.distance;
					});

					var closestSite = selectedSites[0];
					var sitesWithin = selectedSites.filter( function (el) {
						return el.distance < 10;
					});

					$("#all-results").text(selectedSiteIds.length + " sites match your search");
					$("#local-results").text(sitesWithin.length + " sites are within 10 miles" );

					// setView( closestSite.lat, closestSite.lng );
					// drawBuffer(closestSite.lat, closestSite.lng, 10);

					if (sitesWithin.length > 0) {
						updateMarker(closestSite);
						moveMarker(closestSite.id);
					}
					else {
						hideDetails();
					}

					return;
				}

				function setView (lat, lng) {
					map.invalidateSize();
					if (screen.mobile) {
						map.setView([ lat, lng ],9, { animate: false });
						var originPoint = L.latLng(lat, lng);
						var x = map.latLngToContainerPoint( originPoint ).x - 0;
						var y = map.latLngToContainerPoint( originPoint ).y + 40;
						var point = map.containerPointToLatLng([ x, y ]);
						map.setView([ point.lat, point.lng ],9, { animate: false });
					}
					else {
						map.setView([ lat, lng ],9);
					}
					return;
				}

				function moveMarker(id) {

					if( !screen.mobile ) {
						var mapoffset = L.DomUtil.getPosition(map.getPanes().mapPane);
						var site = getSiteObject(id);
						var marker = d3.select("#site-detail");

						marker.style({ display: "block" });

						var height = document.getElementById('site-detail').clientHeight;

						height = parseInt(height) ? parseInt(height) : 0;

						var leftOffset = ( mapoffset.x + projectPoint(site.lng, 0).x - 150 ) + "px";
						var topOffset = ( mapoffset.y + projectPoint(0, site.lat).y - height - 25 ) + "px";

						marker.style({ left: leftOffset, top: topOffset });
					}
				}

				function updateMarker(siteObject) {

					popupSite = siteObject;

					var detail = d3.select("#site-detail");

					// detail.style("height", function(){ return detail.style("height"); });

					detail.select(".details-name").text(siteObject.name);
					detail.select(".details-place").html(siteObject.city + ", " + siteObject.state + "<span id='stars'> &nbsp;&nbsp;&nbsp;" + generateStars(siteObject.star) + "<div id='gia-map-question' class='gia-circle-button'>?</div></span>");
					detail.select(".details-population").text(siteObject.population);
					detail.select(".details-human-exposure").text(siteObject.exposure);
					detail.select(".details-pump").text(siteObject.pump);
				}

				function getDistance(lat1, lon1, lat2, lon2){
					var R = 3959; // mi
					var dLat = toRad( lat2 - lat1 );
					var dLon = toRad(lon2 - lon1 );
					lat1 = toRad ( lat1 );
					lat2 = toRad ( lat2 );

					var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
					var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
					var d = R * c;

					return d;
				}

				/*
				function highlightClosest(closest) {

					points.selectAll("circle")
						.transition()
						.attr("class", "site")
						.attr("r", "1");

					var id = "#S" + closest;

					var closestSite = d3.select("#points").select(id);
					closestSite
						.transition()
						.attr("class", "site highlight")
						.attr("r", "500");
				}
				*/

				function drawBuffer(lat, long, distance){
					var intervals = 18;
					var intervalAngle = (360 / intervals);
					var pointsData = [];

					var originPoint = findPointFromCenter(lat, long, -180, distance);

					for(var i = 1; i < intervals; i++){
						pointsData.push(findPointFromCenter(lat, long, i * intervalAngle - 180, distance));
					}

					g2.selectAll("path")
						.data([pointsData])
					.enter().append("path")
						.attr("d", function(d){ line = lineFunction("basis"); return line(d); })
						.attr("stroke", "#CCC")
						.attr("stroke-width", 1.5)
						.attr("fill", "none");

					g2.selectAll("text")
						.data([originPoint])
					.enter().append("text")
						.attr("x", function(d) { return projectPoint(d[1], 0).x; })
						.attr("y", function(d) { return projectPoint(0, d[0]).y; })
						.attr("font-family", "'Guardian Egyptian Headline', sans-serif")
						.style("text-anchor", "middle")
						.attr("font-size", function() { return map.getZoom() * 1.75; })
						.attr("fill", "#999")
						.text("10mi");


					return;

					// reset();
				}

				function findPointFromCenter(lat1, lon1, bearing, distance){
					distance = distance / 3959; // km is 6371 mi is 3959
					bearing = toRad( bearing);
					lat1 = toRad( lat1 );
					lon1 = toRad( lon1 );
					var lat2 = Math.asin(Math.sin(lat1) * Math.cos(distance) + Math.cos(lat1) * Math.sin(distance) * Math.cos(bearing) );
					var lon2 = lon1 + Math.atan2(Math.sin(bearing) * Math.sin(distance) * Math.cos(lat1), Math.cos(distance) - Math.sin(lat1) * Math.sin(lat2));
					lon2 = (lon2 + 3 * Math.PI) % (2 * Math.PI) - Math.PI;
					return [ toDeg( lat2 ), toDeg( lon2 ) ];
				}

				function getSelectedFilters(){
					var harzards = [], medias = [];
					$("#hazards > .dropdown-items > .selected").each(function(){
						harzards.push($(this).attr("data-value"));
					});

					$("#medias > .dropdown-items > .selected").each(function(){
						medias.push($(this).attr("data-value"));
					});

					getMatchingSites(harzards,medias);

					highlightedSelected();

					return;
				}

				function getMatchingSites (hazards, medias){
					selectedSiteIds = filterSites(hazards, medias)
						.map(function(el){ return el.id; })
						.filter( getUniques );

					return;

				}

				function filterSites(hazards, medias){

					if ( screen.mobile ) {
						return sites.slice();
					}

					if( hazards.indexOf("any") >= 0 ) {
						hazards.splice(0, 1);
					}

					if( medias.indexOf("any") >= 0 ) {
						medias.splice(0, 1);
					}

					if (hazards.length && medias.length) {
						return contaminates.filter(checkHazard).filter(checkMedias);
					}
					else if (hazards.length) {
						return contaminates.filter(checkHazard);
					}
					else if (medias.length) {
						return contaminates.filter(checkMedias);
					}
					else {
						return contaminates;
					}

					function checkHazard(el){
						return hazards.indexOf(el.hazard) !== -1;
					}

					function checkMedias(el){
						return medias.indexOf(el.media) !== -1;
					}

				}

				function getMenuText(array, incoming){

					if(array.indexOf("any contaminants") >= 0 || array.indexOf("any place") >= 0){
						array.splice(0, 1);
					}

					if (array.indexOf(incoming) === -1) {
						array.push(incoming);
					}
					else {
						array.splice(array.indexOf(incoming), 1);
					}

					if (array.join(", ").length > 25) {
						return array[0] + ", +" + (array.length - 1) + " others";
					}
					else {
						return array.join(", ");
					}
				}

				function runQuery(){
					var address = $("#address").val();
					if (!screen.mobile) {
						getSelectedFilters();
					}
					createRequest(address);
				}

				// listeners

				$("#map")
					.on("mousedown", function(){
						hideDetails();
					});

				$(".navigation")
					.on("mouseover", function(){
						hideDetails();
					});

				$("#map-wrapper")
					.on("click", "#gia-map-question", function (e) {
						hideDetails();
						$(".glossary").addClass("open");
					});

				$("#site-detail")
					.on("mousedown", function (e) {
						e.stopPropagation();
					})
					.on("touchstart", function (e) {
						e.stopPropagation();
					});

				$("#button")
					.on("click", function(){
						runQuery();
					});

				$(".dropdown")
					.on("mouseenter", function(){
						$(this).children(".dropdown-items").addClass("dropped");
					})
					.on("mouseleave", function(){
						$(this).children(".dropdown-items").removeClass("dropped");
					});

				$(document).on("keypress", function(e) {
					if (e.keyCode === 13 && $("#address").is(":focus")) {
						runQuery();
					}
				});

				$(".dropdown-item")
					.on("click", function(){
						var parent = $(this).parents(".dropdown");
						var id = parent.attr("id");
						var array;

						if (id === "hazards") {
							array = selectedContaminants;

						}
						else if (id === "medias") {
							array = selectedMedias;
						}

						if ($(this).attr("data-value") === "any") {
							array = [];
							$(parent).find(".selected").removeClass("selected");
						}

						$(this).toggleClass("selected");

						if (id === "hazards") {
							selectedContaminants = array;

						}
						else if (id === "medias") {
							selectedMedias = array;
						}

						$(parent).find(".dropdown-selected").text(getMenuText( array, $(this).attr("data-menu")) );
					});

				$("#glossary-button").on("click", function () {
					hideDetails();
					$(".glossary").toggleClass("open");
				});

				$(".close-button").on("click", function () {
					$(".glossary").removeClass("open");
				});

			});
		}
	};

	window.app = app; // useful for debugging!

	return app;

});
