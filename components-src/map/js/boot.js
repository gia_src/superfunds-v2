(function () {

    var config, localRequire, launch;

    config = {
        context: 'map',
        baseUrl: '<%= components %>/map/',

        paths: {
            d3: '<%= assets %>/lib/d3.min',
            jquery: '<%= assets %>/lib/jquery.min'
        }
    };

    launch = function ( map ) {
        var target = document.getElementById( 'gia-map' ),
            baseUrl = '<%= projectUrl %>';

        map.launch( target, baseUrl, '<%= assets %>' );
    };

    if ( window.GIA_LOADER_IS_REQUIRE ) {
        localRequire = require.config( config );
        localRequire([ 'map' ], launch );
    } else {
        require( config, [ 'map' ], launch );
    }

}());
