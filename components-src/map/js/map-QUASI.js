define( function ( require ) {

	'use strict';

	var app, d3, $, L, scaffolding, loadJSONP;

	d3 = require( 'd3' );
	$ = require( 'jquery' );
	L = require( 'lib/leaflet.mapbox' );
	scaffolding = require( 'text!./scaffolding.html' );
	loadJSONP = require( 'map_loadJSONP' );

	app = {
		launch: function ( el, baseUrl ) {
			var isMobile;

			app.el = el;
			app.$el = $( el );
			app.baseUrl = baseUrl;

			app.el.innerHTML = scaffolding;

			$( document ).ready( function () {

				var $window = $(window);
				var sites;
				var bounds = [];
				var contaminates;
				var zips;
				var selectedSites = [];
				var selectedSiteIds = [];
				var selectedMedias = ["groundwater"];
				var selectedContaminants = ["VOCs"];
				var location = false;
				var popupSite;
				var line;
				var screen = initScreen();

				//////////

				function initScreen() {
					var width = $window.width();

					var wasMobile = isMobile;

					isMobile = app.el.offsetWidth < 800;

					if ( wasMobile !== isMobile ) {
						if ( isMobile ) {
							app.$el.addClass( "gia-mobile" );
						} else {
							app.$el.removeClass( "gia-mobile" );
						}
					}

					return {
						screenWidth: width,
						mobile: width < 800
					};
				}

				var map = L.mapbox.map('map', 'gia.hccg9p1l', {
					maxZoom: 10,
					minZoom: 2,
					doubleClickZoom: false,
					scrollWheelZoom: false,
					inertia: false
				});

				// templating

				var hover = d3.select("#map-wrapper").append("div")
					.attr("class", "site-details dropshadow")
					.attr("id", "site-detail");

				hover.append("div")
						.attr("class", "details-name");

				hover.append("div")
						.attr("class", "details-place");

				var population = hover.append("div")
						.attr("class", "details-row");

				population.append("div")
						.attr("class", "detail-label")
						.text("POP WITHIN 1 MILE");
				population.append("span")
					.attr("class", "details-population");

				var exposure = hover.append("div")
						.attr("class", "details-row");

				exposure.append("div")
					.attr("class", "detail-label")
					.text("HUMAN EXPOSURE");

				exposure.append("span")
					.attr("class", "details-human-exposure");

				var pump = hover.append("div")
						.attr("class", "details-row");
					pump.append("div")
						.attr("class", "detail-label")
						.text("PUMP AND TREAT");
					pump.append("span")
						.attr("class", "details-pump");


				var svg = d3.select(map.getPanes().overlayPane).append("svg"),
					g = svg.append("g").attr("class", "leaflet-zoom-hide"),
					g2 = svg.append("g").attr("class", "leaflet-zoom-hide");

				///////////////

				loadJSONP( baseUrl + "/<%= versionDir %>/files/map/sites.js", 'loadMapSites', function ( s ) {
					sites = s;
					getContaminates();
				});

				function getContaminates(){
					if( !screen.mobile ){
						loadJSONP( baseUrl + "/<%= versionDir %>/files/map/crunched.js", 'loadMapCSV', function ( data ) {
							var contents = d3.csv.parse( data );
							contaminates = contents;
							contaminates = contaminates.map(function(el){
								el.id = parseInt(el.id);
								return el;
							});
							init();
						}, function ( err ) {
							throw err;
						});
					}
					else {
						init();
					}
				}

				//////////

				function init(){
					sites = sites.map(function(el){
						el.selected = false;
						return el;
					});

					var startingId = 901680;
					// get Site
					var startObject = sites.filter(function(el){
							return el.id === startingId;
						})[0];

					if ( screen.mobile ) {
						map.setView([ startObject.lat, startObject.lng ], 9 );
						getMatchingSites([],[]);
					}
					else {
						map.setView([ startObject.lat, startObject.lng ], 4 );
						getMatchingSites(["VOCs"],["GW"]);
					}

					getBounds();

					g.selectAll("circle")
						.data(sites, function(d) { return d.id; } )
					.enter().append("circle")
						.attr("cx", function(d) { return projectPoint(d.lng, 0).x; } )
						.attr("cy", function(d) { return projectPoint(0, d.lat).y; } )
						.attr("r", function() { return map.getZoom() * 0.65; } )
					.on( "mouseover", function(d){
						d3.select(this)
							.transition()
								.attr("r", function(){ return map.getZoom() * 2; });
						updateMarker(d);
						moveMarker(d.id);
					})
					.on( "mouseout", function(){
						d3.select(this)
							.transition()
								.attr("r", function(){ return map.getZoom() * 0.65; })
								.style("fill", function(d){
									return d.selected ? "#003366" : "#CCCCCC";
								});
					})
					.on( "touchstart", function(d){
						d3.select(this)
							.transition()
								.attr("r", function(){ return map.getZoom() * 2; })
							.transition().delay(500)
								.attr("r", function(){ return map.getZoom() * 0.65; });
						updateMarker(d);
					});

					if ( screen.mobile ) {
						map
							.locate({})
							.on( "locationfound", function ( e ) {
								location = e;
								moveToMobilePoint();
							});
					}
					else {
						map
							.on("viewreset", viewReset)
							.on("zoomstart", hideDetails)
							.on("zoomend", checkZoom)
							.on("moveend", moveEnd );
					}

					reset();

					updateMarker(startObject);
					moveMarker(startingId);
					colorTileSites();
				}

				function moveEnd () {
				}


				function moveMobile () {
				}

				function viewReset () {
					reset();
					moveTileSites();
					// drawBuffer();
				}

				function checkZoom () {
					var bounds = map.getBounds();
					if ( bounds._southWest.lat < popupSite.lat && bounds._southWest.lng < popupSite.lng ) {
						if ( bounds._northEast.lat > popupSite.lat && bounds._northEast.lng > popupSite.lng ) {
							showDetails();
						}
					}
				}

				function moveToMobilePoint () {
					addDistance(location.latitude, location.longitude);
				}

				function getBounds() {

					var longso = {
						min: -176.6525,
						max: -64.885
					};
					var latso = {
						min: 64.823,
						max: 13.43916
					};

					bounds[0] = [ projectPoint( longso.min, 0 ).x - 150, projectPoint( 0, latso.min ).y - 150 ];
					bounds[1] = [ projectPoint( longso.max, 0 ).x + 150, projectPoint( 0, latso.max ).y + 150 ];

					return;
				}

				function moveTileSites () {
					var zoom = map.getZoom();
					g.selectAll("circle")
						.attr("cx", function ( d ) { var projectedPoint = projectPoint( d.lng, 0 ); return projectedPoint.x; } )
						.attr("cy", function ( d ) { var projectedPoint = projectPoint( 0, d.lat ); return projectedPoint.y; } )
					.transition()
						.attr("r", zoom * 0.65);
					return;
				}

				function colorTileSites () {
					if ( !screen.mobile ) {
						g.selectAll("circle")
							.sort(function ( a, b ) {
								return a.selected > b.selected ? 1 : -1;
							})
						.transition()
							.style("fill", function ( d ) {
								return d.selected ? "#005689" : "#CCCCCC";
							});
					}
				}

				function highlightedSelected () {
					sites = sites.map( function ( el ) {
						el.selected = selectedSiteIds.indexOf( el.id ) !== - 1 ? true : false;
						return el;
					});

					selectedSites = sites.filter( function ( el ){
						return el.selected;
					});

					colorTileSites();

					return;
				}

				function reset() {
					getBounds();

					var topLeft = bounds[0];
					var bottomRight = bounds[1];

					svg
						.attr("width", bottomRight[0] - topLeft[0])
						.attr("height", bottomRight[1] - topLeft[1])
						.style("left", topLeft[0] + "px")
						.style("top", topLeft[1] + "px");

					g
						.attr("transform", "translate(" + -topLeft[0] + "," + -topLeft[1] + ")");
					g2
						.attr("transform", "translate(" + -topLeft[0] + "," + -topLeft[1] + ")");

					return;
				}

				function createRequest ( address ) {
					var baseUrl = "http://nominatim.openstreetmap.org/search?q=";
					if ( address && address !== "not found" ) {
						address = address.replace(" ", "+");
						var url = baseUrl + address + "&countrycodes=us&format=json";
						$("#loader").addClass("loading");
						getAddress(url);
					}
				}

				function getAddress ( url ) {
					if ( window.XDomainRequest ) {
						var xdr = new XDomainRequest();
						xdr.onerror = function () { };
						xdr.ontimeout = function () {

						};
						xdr.onprogress = function () { };
						xdr.onload = function() {
							$("#loader").removeClass("loading");
							var result = JSON.parse( xdr.responseText );
							if ( result[0] ) {
								var lat = result[0].lat;
								var lng = result[0].lon;
								setView(lat, lng);
								drawBuffer(lat, lng, 10);
								addDistance(lat, lng);
							}
						};
						xdr.timeout = 5000;
						xdr.open("get", url);
						xdr.send();
					}
					else {
						var request = $.ajax({
							crossDomain: true,
							dataType: "json",
							timeout: 5000,
							type: "GET",
							url: url
						});

						request.always( function() { });

						request.done( function(result) {
							$("#loader").removeClass("loading");

							if ( result[0] ) {
								var lat = result[0].lat;
								var lng = result[0].lon;
								setView( lat, lng );
								drawBuffer( lat, lng, 10 );
								addDistance(lat, lng);
							}
							else {
							}
						});

						request.fail( function(err) {
						});
					}
				}

				function addDistance(lat, lng) {
					sites.forEach( function ( el ) {
						if ( +el.lat > +lat - 2 && +el.lat < +lat + 2 && +el.lng > +lng - 2 && +el.lng < +lng + 2 ) {
							el.distance = getDistance( lat, lng, el.lat, el.lng );
							return el;
						}
						else {
							el.distance = 9999;
							return el;
						}
					});

					getClosest();

					return;
				}

				function getClosest() {
					if ( screen.mobile ) {
						selectedSites = sites.slice();
					}
					else {
						getSelectedFilters();
					}

					selectedSites.sort( function ( a, b ) {
						return a.distance - b.distance;
					});

					var closestSite = selectedSites[ 0 ];
					var sitesWithin = selectedSites.filter( function ( el ) {
						return el.distance < 10;
					});

					console.log( "closest:" + closestSite.distance );
					moveMarker(closestSite.id);
					console.log( sitesWithin );

					$("#all-results").text(selectedSiteIds.length + " sites match your search");
					$("#local-results").text(sitesWithin.length + " sites are within 10 miles" );

					if ( sitesWithin.length > 0 ) {
						updateMarker(closestSite);
						moveMarker(closestSite.id);
					}
					else {
						hideDetails();
					}

					return;
				}

				function setView (lat, lng) {
					map.invalidateSize();
					if (screen.mobile) {
						map.setView([ lat, lng ],9, { animate: false });
						var originPoint = L.latLng(lat, lng);
						var x = map.latLngToContainerPoint( originPoint ).x - 0;
						var y = map.latLngToContainerPoint( originPoint ).y + 40;
						var point = map.containerPointToLatLng([ x, y ]);
						map.setView([ point.lat, point.lng ],9, { animate: false });
					}
					else {
						map.setView([ lat, lng ],9);
					}
					return;
				}

				function moveMarker(id) {

					if( !screen.mobile ) {
						var mapoffset = L.DomUtil.getPosition(map.getPanes().mapPane);
						var site = getSiteObject(id);
						var marker = d3.select("#site-detail");

						marker.style({ display: "block" });

						var height = document.getElementById('site-detail').clientHeight;

						height = parseInt(height) ? parseInt(height) : 0;

						var leftOffset = ( mapoffset.x + projectPoint(site.lng, 0).x - 150 ) + "px";
						var topOffset = ( mapoffset.y + projectPoint(0, site.lat).y - height - 25 ) + "px";

						marker.style({ left: leftOffset, top: topOffset });
					}
				}

				function updateMarker(siteObject) {

					popupSite = siteObject;

					var detail = d3.select("#site-detail");

					// detail.style("height", function(){ return detail.style("height"); });

					detail.select(".details-name").text(siteObject.name);
					detail.select(".details-place").html(siteObject.city + ", " + siteObject.state + "<span id='stars'> &nbsp;&nbsp;&nbsp;" + generateStars(siteObject.star) + "<div id='gia-map-question' class='gia-circle-button'>?</div></span>");
					detail.select(".details-population").text(siteObject.population);
					detail.select(".details-human-exposure").text(siteObject.exposure);
					detail.select(".details-pump").text(siteObject.pump);
				}

				function getDistance (lat1, lon1, lat2, lon2){
					var R = 3959; // mi
					var dLat = toRad( lat2 - lat1 );
					var dLon = toRad(lon2 - lon1 );
					lat1 = toRad ( lat1 );
					lat2 = toRad ( lat2 );

					var a = Math.sin( dLat/2 ) * Math.sin(dLat/2) + Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
					var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
					var d = R * c;

					return d;
				}

				function drawBuffer( lat, long, distance ){

					var intervals = 18;
					var intervalAngle = ( 360 / intervals );
					var pointsData = [];

					var originPoint = findPointFromCenter(lat, long, -180, distance);

					for ( var i = 1; i < intervals; i++ ) {
						pointsData.push(findPointFromCenter(lat, long, i * intervalAngle - 180, distance));
					}

					g2.selectAll("path").remove();
					g2.selectAll("text").remove();

					g2.selectAll("path")
						.data([pointsData] )
					.enter().append("path")
						.attr("d", function ( d ) { var line = lineFunction("basis"); return line( d ); })
						.attr("stroke", "#CCC")
						.attr("stroke-width", 1.5)
						.attr("fill", "none");

					g2.selectAll("text")
						.data([originPoint])
					.enter().append("text")
						.attr("x", function(d) { return projectPoint(d[1], 0).x; })
						.attr("y", function(d) { return projectPoint(0, d[0]).y; })
						.attr("font-family", "'Guardian Egyptian Headline', sans-serif")
						.style("text-anchor", "middle")
						.attr("font-size", function() { return map.getZoom() * 1.75; })
						.attr("fill", "#999")
						.text("10mi");

					return;
				}

				function findPointFromCenter ( lat1, lon1, bearing, distance ){
					distance = distance / 3959; // km is 6371 mi is 3959
					bearing = toRad( bearing);
					lat1 = toRad( lat1 );
					lon1 = toRad( lon1 );
					var lat2 = Math.asin(Math.sin(lat1) * Math.cos(distance) + Math.cos(lat1) * Math.sin(distance) * Math.cos(bearing) );
					var lon2 = lon1 + Math.atan2(Math.sin(bearing) * Math.sin(distance) * Math.cos(lat1), Math.cos(distance) - Math.sin(lat1) * Math.sin(lat2));
					lon2 = (lon2 + 3 * Math.PI) % (2 * Math.PI) - Math.PI;
					return [ toDeg( lat2 ), toDeg( lon2 ) ];
				}

				function getSelectedFilters(){
					var harzards = [], medias = [];
					$("#hazards > .dropdown-items > .selected").each(function(){
						harzards.push($(this).attr("data-value"));
					});

					$("#medias > .dropdown-items > .selected").each(function(){
						medias.push($(this).attr("data-value"));
					});

					getMatchingSites(harzards,medias);

					highlightedSelected();



					return;
				}

				function getMatchingSites ( hazards, medias ){
					selectedSiteIds = filterSites( hazards, medias )
						.map( function ( el ) { return el.id; })
						.filter( getUniques );
				}

				function filterSites(hazards, medias){

					if ( screen.mobile ) {
						return sites.slice();
					}

					if( hazards.indexOf("any") >= 0 ) {
						hazards.splice(0, 1);
					}

					if( medias.indexOf("any") >= 0 ) {
						medias.splice(0, 1);
					}

					if (hazards.length && medias.length) {
						return contaminates.filter(checkHazard).filter(checkMedias);
					}
					else if (hazards.length) {
						return contaminates.filter(checkHazard);
					}
					else if (medias.length) {
						return contaminates.filter(checkMedias);
					}
					else {
						return contaminates;
					}

					function checkHazard(el){
						return hazards.indexOf(el.hazard) !== -1;
					}

					function checkMedias(el){
						return medias.indexOf(el.media) !== -1;
					}

				}

				function getMenuText(array, incoming){

					if(array.indexOf("any contaminants") >= 0 || array.indexOf("any place") >= 0){
						array.splice(0, 1);
					}

					if (array.indexOf(incoming) === -1) {
						array.push(incoming);
					}
					else {
						array.splice(array.indexOf(incoming), 1);
					}

					if (array.join(", ").length > 25) {
						return array[0] + ", +" + (array.length - 1) + " others";
					}
					else {
						return array.join(", ");
					}
				}

				function runQuery(){
					var address = $("#address").val();
					// if ( !screen.mobile ) {
					// 	getSelectedFilters();
					// }
					createRequest( address );
				}

				// utilities

				// d3 utilities

				function lineFunction ( interpoloation ) {
					return d3.svg.line()
						.x(function(d) { return projectPoint(d[1], 0).x; })
						.y(function(d) { return projectPoint(0, d[0]).y; })
						.interpolate(interpoloation);
				}

				// map utilities

				function projectPoint ( x, y ) {
					return map.latLngToLayerPoint( new L.LatLng( y, x ));
				}

				function toRad ( deg ) {
					return deg * Math.PI / 180;
				}

				function toDeg ( rad ) {
					return rad * 180 / Math.PI;
				}

				// array utilities

				function getUniques ( value, index, self ) {
					return self.indexOf(value) === index;
				}

				// string utilities

				function repeat (string, number) {
					return new Array( parseInt( number ) + 1).join( string );
				}

				function generateStars(number){
					if(number && number > 0) {
						return repeat("&#9733; ", number);
					}
					else {
						return "";
					}
				}

				//

				function getSiteObject(id){
					return sites.filter( function ( el ){
						return el.id === id;
					})[0];
				}

				//

				function hideDetails(){
					if ( !screen.mobile ) {
						$("#site-detail").css("display", "none");
					}
				}

				function showDetails(){
					$("#site-detail").css("display", "block");
					if( popupSite && popupSite.id ) {
						moveMarker(popupSite.id);
					}
				}

				// listeners

				$window
					.on("resize", function(){
							screen = initScreen();
							moveMobile();
							if (popupSite) {
								moveMarker(popupSite.id);
							}
						});

				$("#map")
					.on("mousedown", function(){
						hideDetails();
					});

				$(".navigation")
					.on("mouseover", function(){
						hideDetails();
					});

				$("#map-wrapper")
					.on("click", "#gia-map-question", function (e) {
						hideDetails();
						$(".glossary").addClass("open");
					});

				$("#site-detail")
					.on("mousedown", function (e) {
						e.stopPropagation();
					})
					.on("touchstart", function (e) {
						e.stopPropagation();
					});

				$("#button")
					.on("click", function(){
						runQuery();
					});

				$(".dropdown")
					.on("mouseenter", function(){
						$(this).children(".dropdown-items").addClass("dropped");
					})
					.on("mouseleave", function(){
						$(this).children(".dropdown-items").removeClass("dropped");
					});

				$(document).on("keypress", function(e) {
					if (e.keyCode === 13 && $("#address").is(":focus")) {
						runQuery();
					}
				});

				$(".dropdown-item")
					.on("click", function(){
						var parent = $(this).parents(".dropdown");
						var id = parent.attr("id");
						var array;

						if (id === "hazards") {
							array = selectedContaminants;

						}
						else if (id === "medias") {
							array = selectedMedias;
						}

						if ($(this).attr("data-value") === "any") {
							array = [];
							$(parent).find(".selected").removeClass("selected");
						}

						$(this).toggleClass("selected");

						if (id === "hazards") {
							selectedContaminants = array;

						}
						else if (id === "medias") {
							selectedMedias = array;
						}

						$(parent).find(".dropdown-selected").text(getMenuText( array, $(this).attr("data-menu")) );
					});

				$("#glossary-button").on("click", function () {
					hideDetails();
					$(".glossary").toggleClass("open");
				});

				$(".close-button").on("click", function () {
					$(".glossary").removeClass("open");
				});

			});
		}
	};

	window.app = app; // useful for debugging!

	return app;

});
