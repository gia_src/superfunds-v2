(function () {

	var config, localRequire, launch;

	config = {
		context: 'video',
        baseUrl: '<%= components %>/video/',

		paths: {
			Ractive: '<%= assets %>/lib/Ractive.runtime.min'
		}
	};

	launch = function ( video ) {
		var target = document.getElementById( 'gia-video' ),
			baseUrl = '<%= projectUrl %>';

		video.launch( target, baseUrl, '<%= assets %>' );
	};

	if ( window.GIA_LOADER_IS_REQUIRE ) {
		localRequire = require.config( config );
		localRequire([ 'video' ], launch );
	} else {
		require( config, [ 'video' ], launch );
	}

}());
