define( function ( require ) {

	'use strict';

	var app = {
		launch: function ( el, url, assetsUrl ) {
			var VideoView, view;

			VideoView = require( 'rvc!./video-view' );

			view = new VideoView({
				el: el
			});
		}
	};

	return app;

});
