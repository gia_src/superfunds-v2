define( function ( require ) {

	'use strict';

	var app, scaffolding, d3, $;

	scaffolding = require( 'text!./oversight_scaffolding.html' );

	d3 = require( 'd3' );
	$ = require( 'jquery' );

	app = {
		launch: function ( el, url ) {
			app.el = el;
			app.$el = $(el);

			app.el.innerHTML = scaffolding;

			// insert arrows SVG
			app.el.querySelector( '.gia-oversight-arrows' ).src = url + '/<%= versionDir %>/files/oversight/arrows.svg';

			var $window = $(window);
			var width = $window.width();
			$window.on( 'resize', function () {
				width = $window.width();
			});

			var segmentIndex = 0;

			var segmentNames = ["one", "two", "three", "four", "five", "six", "seven"];

			var illustrations = require( 'oversight_data' ).illustrations;

			var svg = d3.select("#illustrations")
				.append("svg")
					.attr("width", 2500)
					.attr("height", 1000);

			segmentNames.forEach(function(el){
				var g = svg.append("g").attr("id", "seg-" + el);

				g.append("path")
					.attr("d", illustrations[el] || 'M0,0')
					.attr("stroke-dasharray", function(){ return 0 + "," + this.getTotalLength(); });
			});

			function drawSegment(reverse){

				reverse = reverse || false;

				function draw(index) {
					index = index || 0;

					var segmentId = "#seg-" + segmentNames[index];

					var test = d3.select("#illustrations").select(segmentId).select("path")
						.call(transition);

					function transition(path) {
						path.transition()
							.duration(1000)
							.attrTween("stroke-dasharray", function(){ var dashPosition = d3.select(this).attr("stroke-dasharray").split(","); return tweenDash(this, dashPosition[0]); });
					}

					function tweenDash(el, dashPosition) {
						var i, l = el.getTotalLength();

						if(!reverse) {
							i = d3.interpolateString(dashPosition + "," + l, l + "," + l);
						}
						else {
							i = d3.interpolateString(dashPosition + "," + l, "0," + l);
						}
						return function(t) { return i(t); };
					}
				}
				return draw;
			}

			var drawForward = drawSegment();
			var drawBackward = drawSegment(true);

			drawForward(0);

			d3.select(window)
				.on("keydown", function() {
					if(d3.event.keyCode === 39){
						next();
					}
					if(d3.event.keyCode === 37){
						previous();
					}
				});

			function next() {
				if (segmentIndex === 4) {
					$("#gia-oversight-next").addClass("disabled-button");
				}

				$("#gia-oversight-previous").removeClass("disabled-button");

				if(segmentIndex < 5){
					drawForward(++segmentIndex);
					moveSegments();
				}
				else if(segmentIndex < 5){
					drawForward(++segmentIndex);
				}
			}
			function previous() {

				if (segmentIndex === 1) {
					$("#gia-oversight-previous").addClass("disabled-button");
				}

				$("#gia-oversight-next").removeClass("disabled-button");

				if(segmentIndex > 1){
					drawBackward(segmentIndex--);
					moveSegments();
				}
				else if(segmentIndex > 0){
					drawBackward(segmentIndex--);
				}
			}

			function moveSegments(){
				var width = $(app.el).width();
				var left;

				var offset = width < 800 ? 0 : 150;
				var offsetNumber = width < 800 ? 0 : 1;
				left = -((segmentIndex - offsetNumber) * 250) + offset + 'px';
				d3.select("#segments").style("left", left );
			}

			$(document).ready(function() {
				app.$el.find("#gia-oversight-previous").on("click", function(){
					previous();
				});
				app.$el.find("#gia-oversight-next").on("click", function(){
					next();
				});
			});
		}
	};

	return app;

});
