(function () {

    var config, localRequire, launch;

    if ( window.innerWidth < 800 ) {
        return;
    }

    config = {
        context: 'oversight',
        baseUrl: '<%= components %>/oversight/',

        paths: {
            d3: '<%= assets %>/lib/d3.min',
            jquery: '<%= assets %>/lib/jquery.min'
        }
    };

    launch = function ( oversight ) {
        var target = document.getElementById( 'gia-oversight' ),
            baseUrl = '<%= projectUrl %>';

        oversight.launch( target, baseUrl, '<%= assets %>' );
    };

    if ( window.GIA_LOADER_IS_REQUIRE ) {
        localRequire = require.config( config );
        localRequire([ 'oversight' ], launch );
    } else {
        require( config, [ 'oversight' ], launch );
    }

}());
