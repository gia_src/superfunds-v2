define( function ( require ) {

	'use strict';

	var data = require( 'data' ),
		csvToJson = require( 'utils/data/csvToJson' ),
		reindex = require( 'utils/data/reindex' );

	data.sites = csvToJson( data.sites );
	data.siteById = reindex( data.sites, 'id' );

	data.journeys = data.journeys.map( function ( journey ) {
		return journey.map( hydrateHash );
	});

	return data;


	function hydrateHash ( hash ) {
		var journey = {}, split, source_id, dest_id;

		split = hash.split( '/' );
		source_id = split[0];
		dest_id = split[1];

		journey.from = data.siteById[ source_id ];
		journey.to = data.siteById[ dest_id ];

		return journey;
	}

});
