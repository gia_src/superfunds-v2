define( function ( require ) {

	'use strict';

	var app = {
		launch: function ( el, url ) {
			app.el = el;
			app.url = url;

			app.data = require( 'helpers/unpackedData' );

			// init modules
			require( 'app/storyboard/_storyboard' ).init( this );
			require( 'app/scrollwatcher/_scrollwatcher' ).init( this );
		}
	};

	require( 'utils/core/extend' )( app, require( 'utils/core/events' ) );

	return app;

});
