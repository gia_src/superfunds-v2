define( function ( require ) {

	'use strict';

	var data = require( 'helpers/unpackedData' );

	return {
		init: function ( app ) {

			var Storyboard, view;

			Storyboard = require( 'rvc!./storyboard' );

			view = new Storyboard({
				el: app.el,

				debug: true,

				data: {
					appUrl: app.url,

					MEW: data.siteById.MEW,
					CALGON: data.siteById.KYD005009923,
					CALGON_NEVILLE: data.siteById.PAD000736942,
					NORIT: data.siteById.OKD987072006,
					SIEMENS_1: data.siteById.AZD982441263,
					SIEMENS_REDBLUFF: data.siteById.CAR000058784,

					journeys: app.data.journeys,
					segments: [
						{ start: 0, end: 0 },
						{ start: 0, end: 0 },
						{ start: 0, end: 0 }
					],
					showDest: [ false, false, false ]
				},

				scenes: {
					resting: {
						enter: function () {

						}
					},

					step1: function () {
						var storyboard = this, animation;

						return {
							enter: function () {
								// current state
								storyboard.set({
									segments: [
										{ start: 0, end: 0 },
										{ start: 0, end: 0 },
										{ start: 0, end: 0 }
									],
									showDest: [ false, false, false ]
								});

								animation = storyboard.animate( 'segments[0].end', 1, {
									duration: 6000,
									easing: 'linear',
									complete: function () {
										storyboard.set( 'showDest[0]', true );

										storyboard.animate( 'segments[0].start', 1, {
											duration: 2000,
											easing: 'linear',
											complete: function () {
												storyboard.goto( 'step2' );
											}
										});
									}
								});
							},

							exit: function () {
								animation.stop();
							}
						};
					},

					step2: function () {
						var storyboard = this, animation;

						return {
							enter: function () {
								// current state
								storyboard.set({
									segments: [
										{ start: 0, end: 0 },
										{ start: 0, end: 0 },
										{ start: 0, end: 0 }
									],
									showDest: [ true, false, false ]
								});

								animation = this.animate( 'segments[1].end', 1, {
									duration: 8000,
									easing: 'linear',
									complete: function () {
										storyboard.set( 'showDest[1]', true );
										storyboard.goto( 'step3' );
									}
								});
							},

							exit: function () {
								animation.stop();
							}
						}
					},

					step3: function () {
						var storyboard = this, animation;

						return {
							enter: function () {
								// current state
								storyboard.set({
									segments: [
										{ start: 0, end: 0 },
										{ start: 0, end: 1 },
										{ start: 0, end: 0 }
									],
									showDest: [ true, true, false ]
								});

								animation = storyboard.animate( 'segments[2].end', 1, {
									duration: 8000,
									easing: 'linear',
									complete: function () {
										storyboard.set( 'showDest[2]', true );
									}
								});
							},

							exit: function () {
								animation.stop();
							}
						}
					}
				}
			});

			app.bind( 'start', function () {
				view.goto( 'step1' );
			});

			view.on( 'replay', function () {
				app.trigger( 'start' );
			});

			window.toxictrail = view;
		}
	};

});
