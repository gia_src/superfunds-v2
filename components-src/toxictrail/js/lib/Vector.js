define([

], function (

) {

	'use strict';

	var Vector = function ( x, y ) {
		this.x = x;
		this.y = y;
	};

	Vector.prototype = {
		plus: function ( vector2 ) {
			return new Vector( this.x + vector2.x, this.y + vector2.y );
		},

		minus: function ( vector2 ) {
			return new Vector( this.x - vector2.x, this.y - vector2.y );
		},

		times: function ( s ) {
			return new Vector( s * this.x, s * this.y );
		},

		toString: function () {
			return this.x + ',' + this.y;
		}
	};

	return Vector;

});
