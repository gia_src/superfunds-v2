define( function ( require ) {

	'use strict';

	var Line = require( 'lib/Line' ),
		Vector = require( 'lib/Vector' );

	var Curve = function ( p0, p1, p2 ) {
		this.p0 = new Vector( p0.x, p0.y );
		this.p1 = new Vector( p1.x, p1.y );
		this.p2 = new Vector( p2.x, p2.y );

		this.p0_p1 = this.p1.minus( this.p0 );
		this.p1_p2 = this.p2.minus( this.p1 );
	};

	Curve.prototype = {
		segment: function ( t0, t1 ) {
			var q0_t0, q1_t0, q0_t1, q1_t1, q0_t0__q1_t0, q0_t1__q1_t1, p0, p1, p2, curve;

			q0_t0 = this.p0.plus( this.p0_p1.times( t0 ) );
			q1_t0 = this.p1.plus( this.p1_p2.times( t0 ) );

			q0_t1 = this.p0.plus( this.p0_p1.times( t1 ) );
			q1_t1 = this.p1.plus( this.p1_p2.times( t1 ) );

			p0 = q0_t0.plus( q1_t0.minus( q0_t0 ).times( t0 ) );
			p2 = q0_t1.plus( q1_t1.minus( q0_t1 ).times( t1 ) );

			q0_t0__q1_t0 = new Line( q0_t0, q1_t0 );
			q0_t1__q1_t1 = new Line( q0_t1, q1_t1 );

			// control (p1) is the intersection of q0_t0__q1_t0 and q0_t1__q1_t1
			p1 = q0_t0__q1_t0.intersection( q0_t1__q1_t1 ) || p0;

			curve = new Curve( p0, p1, p2 );
			return curve;
		},

		at: function ( t ) {
			var p1 = this.p0.plus( this.p0_p1.times( t ) ),
				p2 = this.p1.plus( this.p1_p2.times( t ) ),
				childCurve;

			childCurve = new Curve( this.p0, p2, p1 );

			return childCurve;
		},

		toString: function () {
			return 'M' + this.p0 + 'Q' + this.p1 + ' ' + this.p2;
		}
	};

	return Curve;

});
