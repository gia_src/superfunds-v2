(function ( global, factory ) {

	'use strict';

	// Common JS (i.e. browserify) environment
	if ( typeof module !== 'undefined' && module.exports && typeof require === 'function' ) {
		factory( require( 'Ractive' ) );
	}

	// AMD?
	else if ( typeof define === 'function' && define.amd ) {
		define([ 'Ractive' ], factory );
	}

	// browser global
	else if ( global.Ractive ) {
		factory( global.Ractive );
	}

	else {
		throw new Error( 'Could not find Ractive! It must be loaded before the Ractive-transitions-draw plugin' );
	}

}( typeof window !== 'undefined' ? window : this, function ( Ractive ) {

	'use strict';

	Ractive.transitions.draw = function ( t, params ) {

		var length;

		if ( !t.node.getTotalLength ) {
			throw new Error( 'You cannot use the draw transition with non-SVG path elements' );
		}

		length = t.node.getTotalLength();
		params = t.processParams( params, {
			easing: 'easeInOut',
			duration: ( params && params.speed !== undefined ? length / params.speed : 600 )
		});

		if ( t.isIntro ) {
			t.setStyle({
				strokeDasharray: length + ' ' + length,
				strokeDashoffset: params.reverse ? -length : length
			});

			t.animateStyle( 'strokeDashoffset', 0, params ).then( t.complete );
		}

		else {
			t.setStyle({
				strokeDasharray: length + ' ' + length
			});

			t.animateStyle( 'strokeDashoffset', ( params.reverse ? -length : length ), params ).then( t.complete );
		}
	};

}));
