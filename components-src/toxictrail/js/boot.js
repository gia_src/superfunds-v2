(function () {

	var config, localRequire, launch;

	config = {
		context: 'toxictrail',
        baseUrl: '<%= components %>/toxictrail/',

		paths: {
			Ractive: '<%= assets %>/lib/Ractive.runtime.min'
		}
	};

	launch = function ( toxictrail ) {
		var target = document.getElementById( 'gia-toxictrail' ),
			baseUrl = '<%= projectUrl %>';

		toxictrail.launch( target, baseUrl, '<%= assets %>' );
	};

	if ( window.GIA_LOADER_IS_REQUIRE ) {
		localRequire = require.config( config );
		localRequire([ 'toxictrail' ], launch );
	} else {
		require( config, [ 'toxictrail' ], launch );
	}

}());
